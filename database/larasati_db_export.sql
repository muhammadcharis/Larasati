/*
 Navicat Premium Data Transfer

 Source Server         : larasati
 Source Server Type    : PostgreSQL
 Source Server Version : 90502
 Source Host           : localhost:5432
 Source Catalog        : larasatiblora_db
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 90502
 File Encoding         : 65001

 Date: 11/10/2018 08:02:12
*/


-- ----------------------------
-- Sequence structure for admins_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."admins_id_seq";
CREATE SEQUENCE "public"."admins_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for migrations_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."migrations_id_seq";
CREATE SEQUENCE "public"."migrations_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for users_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."users_id_seq";
CREATE SEQUENCE "public"."users_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for admins
-- ----------------------------
DROP TABLE IF EXISTS "public"."admins";
CREATE TABLE "public"."admins" (
  "id" int4 NOT NULL DEFAULT nextval('admins_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "password" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "is_active" bool NOT NULL DEFAULT true,
  "remember_token" varchar(100) COLLATE "pg_catalog"."default",
  "deleted_at" timestamp(0),
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;
ALTER TABLE "public"."admins" OWNER TO "postgres";

-- ----------------------------
-- Records of admins
-- ----------------------------
BEGIN;
INSERT INTO "public"."admins" VALUES (1, 'Admin Larasati Blora', 'admin@larasatiblora.com', '$2y$10$cjy8IHHrMOB5GLIfCdivO.5gmuFKzETGo9uJNkd200HGe5ndbNkAu', 't', '9BkqYNqhDaHHcF5EiuKapaAZQ590OYX7OAuGYb9jmoGP2JR7KA8sjd6zgKWY', NULL, '2018-06-01 05:59:29', '2018-06-01 05:59:29');
COMMIT;

-- ----------------------------
-- Table structure for appointments
-- ----------------------------
DROP TABLE IF EXISTS "public"."appointments";
CREATE TABLE "public"."appointments" (
  "id" char(36) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "no_tlpn" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "date_appointment" date NOT NULL,
  "appointment_type" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "description" text COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;
ALTER TABLE "public"."appointments" OWNER TO "postgres";

-- ----------------------------
-- Table structure for areas
-- ----------------------------
DROP TABLE IF EXISTS "public"."areas";
CREATE TABLE "public"."areas" (
  "id" char(36) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "description" text COLLATE "pg_catalog"."default",
  "admin_id" int4 NOT NULL,
  "order_by" int4 NOT NULL DEFAULT 1,
  "is_show" bool NOT NULL DEFAULT true,
  "deleted_at" timestamp(0),
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;
ALTER TABLE "public"."areas" OWNER TO "postgres";

-- ----------------------------
-- Records of areas
-- ----------------------------
BEGIN;
INSERT INTO "public"."areas" VALUES ('f25d9df0-6560-11e8-83f4-c78a7b7d9bff', 'AREA GERBANG UTAMA', NULL, 1, 1, 't', NULL, '2018-06-01 05:59:29', '2018-06-01 05:59:29');
INSERT INTO "public"."areas" VALUES ('f25ed6e0-6560-11e8-a9c4-1d85628872f8', 'AREA PENERIMA TAMU LOBY', NULL, 1, 2, 't', NULL, '2018-06-01 05:59:29', '2018-06-01 05:59:29');
INSERT INTO "public"."areas" VALUES ('f25fa900-6560-11e8-8b8f-fb0fe2dc7f7a', 'AREA PHOTOBOOTH LOBY', NULL, 1, 3, 't', NULL, '2018-06-01 05:59:29', '2018-06-01 05:59:29');
INSERT INTO "public"."areas" VALUES ('f2605570-6560-11e8-b956-63889838d088', 'AREA JALUR TAMU VIP & REGULER', NULL, 1, 4, 't', NULL, '2018-06-01 05:59:29', '2018-06-01 05:59:29');
INSERT INTO "public"."areas" VALUES ('f2613c60-6560-11e8-aeed-43f348c71c53', 'AREA PANGGUNG MUSIK', NULL, 1, 5, 't', NULL, '2018-06-01 05:59:29', '2018-06-01 05:59:29');
INSERT INTO "public"."areas" VALUES ('f2622490-6560-11e8-9b98-6f763772e0e9', 'AREA PELAMINAN', NULL, 1, 6, 't', NULL, '2018-06-01 05:59:29', '2018-06-01 05:59:29');
INSERT INTO "public"."areas" VALUES ('f262eed0-6560-11e8-9242-e177cd0bd818', 'AREA AKAD NIKAH', NULL, 1, 7, 't', NULL, '2018-06-01 05:59:29', '2018-06-01 05:59:29');
INSERT INTO "public"."areas" VALUES ('f263a1c0-6560-11e8-a50b-334b500fc2d7', 'LAIN - LAIN', NULL, 1, 8, 'f', NULL, '2018-06-01 05:59:29', '2018-06-01 05:59:29');
INSERT INTO "public"."areas" VALUES ('f2644800-6560-11e8-a4e2-718258a6debb', 'NOTE', NULL, 1, 9, 'f', NULL, '2018-06-01 05:59:29', '2018-06-01 05:59:29');
COMMIT;

-- ----------------------------
-- Table structure for blogs
-- ----------------------------
DROP TABLE IF EXISTS "public"."blogs";
CREATE TABLE "public"."blogs" (
  "id" char(36) COLLATE "pg_catalog"."default" NOT NULL,
  "title" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "slug" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "content" text COLLATE "pg_catalog"."default" NOT NULL,
  "published_date" varchar(255) COLLATE "pg_catalog"."default",
  "status" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "featured_image" varchar(255) COLLATE "pg_catalog"."default",
  "featured_mimetype" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;
ALTER TABLE "public"."blogs" OWNER TO "postgres";

-- ----------------------------
-- Table structure for galleries
-- ----------------------------
DROP TABLE IF EXISTS "public"."galleries";
CREATE TABLE "public"."galleries" (
  "id" char(36) COLLATE "pg_catalog"."default" NOT NULL,
  "service_id" char(36) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "description" text COLLATE "pg_catalog"."default" NOT NULL,
  "filename_mimetype" varchar(255) COLLATE "pg_catalog"."default",
  "filename" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;
ALTER TABLE "public"."galleries" OWNER TO "postgres";

-- ----------------------------
-- Records of galleries
-- ----------------------------
BEGIN;
INSERT INTO "public"."galleries" VALUES ('da0ccb30-c9d0-11e8-9482-09ebd9344715', 'a4870dd0-c9cf-11e8-8d69-91f51975be3d', 'asdasdasd', 'asdasdasdasdasdasdasdasdasdasdassd', 'image/jpeg', 'yeByFNAWxQRSSSr8VD7FEIwuzBlrqvXv.jpg', '2018-10-07 01:32:28', '2018-10-07 01:32:28');
INSERT INTO "public"."galleries" VALUES ('8746d540-c9d2-11e8-bbce-bd70cfa7be56', 'a4870dd0-c9cf-11e8-8d69-91f51975be3d', 'sadsadasd', 'xasxasdsdasdasdsa', 'image/jpeg', 'DedA2JpUI4swsPsBqnYt5CCrIkqJtQAS.jpg', '2018-10-07 01:44:28', '2018-10-07 01:44:28');
INSERT INTO "public"."galleries" VALUES ('97eb4230-c9d2-11e8-baff-db1de684bb93', 'b35aa4e0-c9cf-11e8-9bbb-9965d4567520', 'xxxx', 'asddsscd', 'image/jpeg', 'driNocJGk6g9NUTVZcTVEItArYv88REb.jpg', '2018-10-07 01:44:56', '2018-10-07 01:44:56');
INSERT INTO "public"."galleries" VALUES ('ae228520-c9d2-11e8-8b50-d1ab710dc7e6', 'b35aa4e0-c9cf-11e8-9bbb-9965d4567520', 'xxxxvvv1231', 'asdasdas', 'image/jpeg', 'Nl0EWTcglxN9s2JeejP1FX0AqHOO2mzl.jpg', '2018-10-07 01:45:33', '2018-10-07 01:45:33');
INSERT INTO "public"."galleries" VALUES ('6e205f90-c9d3-11e8-907f-a5ac35e589b1', 'a4870dd0-c9cf-11e8-8d69-91f51975be3d', '12312', '3123123', 'image/jpeg', 'EVvK3R01AlkiZJ6Uk2FEv12tJR9RSSc0.jpeg', '2018-10-07 01:50:55', '2018-10-07 01:50:55');
COMMIT;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS "public"."migrations";
CREATE TABLE "public"."migrations" (
  "id" int4 NOT NULL DEFAULT nextval('migrations_id_seq'::regclass),
  "migration" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "batch" int4 NOT NULL
)
;
ALTER TABLE "public"."migrations" OWNER TO "postgres";

-- ----------------------------
-- Records of migrations
-- ----------------------------
BEGIN;
INSERT INTO "public"."migrations" VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO "public"."migrations" VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO "public"."migrations" VALUES (3, '2018_03_31_140228_create_admins_table', 1);
INSERT INTO "public"."migrations" VALUES (4, '2018_04_02_142947_create_areas_table', 1);
INSERT INTO "public"."migrations" VALUES (5, '2018_05_18_122956_create_sliders_table', 1);
INSERT INTO "public"."migrations" VALUES (6, '2018_05_22_132922_create_services_table', 1);
INSERT INTO "public"."migrations" VALUES (7, '2018_05_27_024055_create_timelines_table', 1);
INSERT INTO "public"."migrations" VALUES (8, '2018_06_01_061511_create_partners_table', 2);
INSERT INTO "public"."migrations" VALUES (9, '2018_06_01_061525_create_blogs_table', 2);
INSERT INTO "public"."migrations" VALUES (10, '2018_06_01_061535_create_vanues_table', 2);
INSERT INTO "public"."migrations" VALUES (11, '2018_06_01_061544_create_galleries_table', 2);
INSERT INTO "public"."migrations" VALUES (12, '2018_06_05_124642_create_vanue_galleries_table', 2);
INSERT INTO "public"."migrations" VALUES (13, '2018_06_05_124705_create_vanue_packages_table', 2);
INSERT INTO "public"."migrations" VALUES (14, '2018_06_05_125209_create_appointments_table', 2);
INSERT INTO "public"."migrations" VALUES (15, '2018_10_01_121343_create_orders_table', 3);
INSERT INTO "public"."migrations" VALUES (16, '2018_10_01_122004_create_order_categories_table', 3);
INSERT INTO "public"."migrations" VALUES (17, '2018_10_01_123351_create_order_details_table', 3);
COMMIT;

-- ----------------------------
-- Table structure for order_categories
-- ----------------------------
DROP TABLE IF EXISTS "public"."order_categories";
CREATE TABLE "public"."order_categories" (
  "id" char(36) COLLATE "pg_catalog"."default" NOT NULL,
  "order_id" char(36) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "total_per_category" numeric(15,2) DEFAULT '0'::numeric,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;
ALTER TABLE "public"."order_categories" OWNER TO "postgres";

-- ----------------------------
-- Table structure for order_details
-- ----------------------------
DROP TABLE IF EXISTS "public"."order_details";
CREATE TABLE "public"."order_details" (
  "id" char(36) COLLATE "pg_catalog"."default" NOT NULL,
  "order_category_id" char(36) COLLATE "pg_catalog"."default" NOT NULL,
  "detail_description" text COLLATE "pg_catalog"."default" NOT NULL,
  "qty" int4 NOT NULL DEFAULT 0,
  "price" numeric(15,2) DEFAULT '0'::numeric,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;
ALTER TABLE "public"."order_details" OWNER TO "postgres";

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS "public"."orders";
CREATE TABLE "public"."orders" (
  "id" char(36) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "vanue" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "order_date_from" date NOT NULL,
  "order_number" char(15) COLLATE "pg_catalog"."default" NOT NULL,
  "total_order" numeric(15,2) DEFAULT '0'::numeric,
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "order_date_to" date
)
;
ALTER TABLE "public"."orders" OWNER TO "postgres";

-- ----------------------------
-- Table structure for partners
-- ----------------------------
DROP TABLE IF EXISTS "public"."partners";
CREATE TABLE "public"."partners" (
  "id" char(36) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "web_url" varchar(255) COLLATE "pg_catalog"."default",
  "filename_mimetype" varchar(255) COLLATE "pg_catalog"."default",
  "filename" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;
ALTER TABLE "public"."partners" OWNER TO "postgres";

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS "public"."password_resets";
CREATE TABLE "public"."password_resets" (
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "token" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamp(0)
)
;
ALTER TABLE "public"."password_resets" OWNER TO "postgres";

-- ----------------------------
-- Table structure for services
-- ----------------------------
DROP TABLE IF EXISTS "public"."services";
CREATE TABLE "public"."services" (
  "id" char(36) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "description" text COLLATE "pg_catalog"."default",
  "filename_mimetype" varchar(255) COLLATE "pg_catalog"."default",
  "filename" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "slug" varchar(255) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."services" OWNER TO "postgres";

-- ----------------------------
-- Records of services
-- ----------------------------
BEGIN;
INSERT INTO "public"."services" VALUES ('a4870dd0-c9cf-11e8-8d69-91f51975be3d', 'modern altar', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit ess', 'image/jpeg', 'eOxt1DFrTvyYMOYfzfHeuQBBIvtlEMsx.jpg', '2018-10-07 01:23:49', '2018-10-07 01:23:49', 'modern-altar');
INSERT INTO "public"."services" VALUES ('b35aa4e0-c9cf-11e8-9bbb-9965d4567520', 'table decoration', 'Integer eu diam nunc. Quisque nec mollis nisi. In eget magna at nisi condimentum interdum ut non erat. Nulla ut iaculis mauris, vel ultricies ligula. Phasellus a tincidunt risus. Morbi finibus massa quis libero vestibulum interdum. Sed vel sapien sed arcu lobortis blandit. Donec porttitor vulputate ligula eu consectetur. Vestibulum varius turpis ut dignissim accumsan.', 'image/jpeg', 'O2g1wXaEP9FKPe4p40yhY3D4vVoUVUn2.jpg', '2018-10-07 01:24:13', '2018-10-07 01:24:13', 'table-decoration');
INSERT INTO "public"."services" VALUES ('5f85bde0-c9d8-11e8-a9bd-934786a7c1af', 'asdasdasd', 'asdasdasdas', 'image/png', 'QCyhdqdzUk3hcdIjT3dTe7XN70fx5GLj.png', '2018-10-07 02:26:18', '2018-10-07 02:26:18', 'asdasdasd');
INSERT INTO "public"."services" VALUES ('8b2ea770-c9d8-11e8-b78d-8d2186aafeca', 'asxasx', 'asdasdsa', 'image/png', 'PPEBBdXbggSIKT7zr4WlWFRFSgEz8qoS.png', '2018-10-07 02:27:32', '2018-10-07 02:27:32', 'asxasx');
COMMIT;

-- ----------------------------
-- Table structure for sliders
-- ----------------------------
DROP TABLE IF EXISTS "public"."sliders";
CREATE TABLE "public"."sliders" (
  "id" char(36) COLLATE "pg_catalog"."default" NOT NULL,
  "description" text COLLATE "pg_catalog"."default",
  "filename_mimetype" varchar(255) COLLATE "pg_catalog"."default",
  "filename" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;
ALTER TABLE "public"."sliders" OWNER TO "postgres";

-- ----------------------------
-- Records of sliders
-- ----------------------------
BEGIN;
INSERT INTO "public"."sliders" VALUES ('7183ae00-c9cd-11e8-9337-49237e0f2c16', 'wkwkwkkwkw', 'image/jpeg', 'HhYMlhgIvi7s4chbzq6cCiyJ87HDpqLh.jpg', '2018-10-07 01:08:04', '2018-10-07 01:08:04');
COMMIT;

-- ----------------------------
-- Table structure for timelines
-- ----------------------------
DROP TABLE IF EXISTS "public"."timelines";
CREATE TABLE "public"."timelines" (
  "id" char(36) COLLATE "pg_catalog"."default" NOT NULL,
  "description" text COLLATE "pg_catalog"."default" NOT NULL,
  "timeline_date" date NOT NULL,
  "filename_mimetype" varchar(255) COLLATE "pg_catalog"."default",
  "filename" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;
ALTER TABLE "public"."timelines" OWNER TO "postgres";

-- ----------------------------
-- Records of timelines
-- ----------------------------
BEGIN;
INSERT INTO "public"."timelines" VALUES ('7483d7f0-6b22-11e8-be92-0337bb0c029c', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '1980-02-01', 'image/jpeg', 'cUXF9kyc6XBu1p6Ix5awrJvh5mGMl4rG.jpg', '2018-06-08 13:47:16', '2018-06-08 13:47:16');
INSERT INTO "public"."timelines" VALUES ('b726e550-6b22-11e8-aa67-9fa6659843a5', 'Mauris a sollicitudin nunc. Ut iaculis, sapien eu porttitor aliquet, libero lorem tempus odio, ac pellentesque arcu purus nec tellus. Phasellus congue dolor non malesuada facilisis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque quis dapibus lacus. Nulla vestibulum leo ac lorem dignissim ullamcorper. Sed eget tempus ex, id cursus leo. Fusce sed risus lectus. Nulla facilisi. Mauris non nulla laoreet, euismod orci sit amet, tempus nibh. Mauris ut fringilla lectus, sit amet scelerisque dui.', '1991-09-16', 'image/jpeg', 'fxX9u9KmAM6SQZKlrQsNPkvM1GJ8lEiP.jpg', '2018-06-08 13:49:08', '2018-06-08 13:49:08');
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
  "id" int4 NOT NULL DEFAULT nextval('users_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "password" varchar(255) COLLATE "pg_catalog"."default" NOT NULL DEFAULT '12345678'::character varying,
  "address" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "phone" varchar(255) COLLATE "pg_catalog"."default" NOT NULL DEFAULT '12345678'::character varying,
  "is_active" bool NOT NULL DEFAULT false,
  "remember_token" varchar(100) COLLATE "pg_catalog"."default",
  "deleted_at" timestamp(0),
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;
ALTER TABLE "public"."users" OWNER TO "postgres";

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO "public"."users" VALUES (1, 'Nessya', 'nessyalvioriza@gmail.com', '$2y$10$bM6DGWxFxMMgdy1iZk25S.tdz73YXlpXPwwjtvCHZsVmjUQhwiQam', 'Jl. Perkutut no 3', '081294633942', 't', NULL, NULL, '2018-06-01 05:59:29', '2018-06-01 05:59:29');
COMMIT;

-- ----------------------------
-- Table structure for vanue_galleries
-- ----------------------------
DROP TABLE IF EXISTS "public"."vanue_galleries";
CREATE TABLE "public"."vanue_galleries" (
  "id" char(36) COLLATE "pg_catalog"."default" NOT NULL,
  "vanue_id" char(36) COLLATE "pg_catalog"."default" NOT NULL,
  "description" text COLLATE "pg_catalog"."default" NOT NULL,
  "filename_mimetype" varchar(255) COLLATE "pg_catalog"."default",
  "filename" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;
ALTER TABLE "public"."vanue_galleries" OWNER TO "postgres";

-- ----------------------------
-- Table structure for vanue_packages
-- ----------------------------
DROP TABLE IF EXISTS "public"."vanue_packages";
CREATE TABLE "public"."vanue_packages" (
  "id" char(36) COLLATE "pg_catalog"."default" NOT NULL,
  "vanue_id" char(36) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;
ALTER TABLE "public"."vanue_packages" OWNER TO "postgres";

-- ----------------------------
-- Table structure for vanues
-- ----------------------------
DROP TABLE IF EXISTS "public"."vanues";
CREATE TABLE "public"."vanues" (
  "id" char(36) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "description" text COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;
ALTER TABLE "public"."vanues" OWNER TO "postgres";

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."admins_id_seq"
OWNED BY "public"."admins"."id";
SELECT setval('"public"."admins_id_seq"', 2, true);
ALTER SEQUENCE "public"."migrations_id_seq"
OWNED BY "public"."migrations"."id";
SELECT setval('"public"."migrations_id_seq"', 18, true);
ALTER SEQUENCE "public"."users_id_seq"
OWNED BY "public"."users"."id";
SELECT setval('"public"."users_id_seq"', 2, true);

-- ----------------------------
-- Uniques structure for table admins
-- ----------------------------
ALTER TABLE "public"."admins" ADD CONSTRAINT "admins_email_unique" UNIQUE ("email");

-- ----------------------------
-- Primary Key structure for table admins
-- ----------------------------
ALTER TABLE "public"."admins" ADD CONSTRAINT "admins_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table appointments
-- ----------------------------
ALTER TABLE "public"."appointments" ADD CONSTRAINT "appointments_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table areas
-- ----------------------------
ALTER TABLE "public"."areas" ADD CONSTRAINT "areas_name_unique" UNIQUE ("name");

-- ----------------------------
-- Primary Key structure for table areas
-- ----------------------------
ALTER TABLE "public"."areas" ADD CONSTRAINT "areas_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table blogs
-- ----------------------------
ALTER TABLE "public"."blogs" ADD CONSTRAINT "blogs_slug_unique" UNIQUE ("slug");

-- ----------------------------
-- Checks structure for table blogs
-- ----------------------------
ALTER TABLE "public"."blogs" ADD CONSTRAINT "blogs_status_check" CHECK (((status)::text = ANY ((ARRAY['draft'::character varying, 'published'::character varying])::text[])));

-- ----------------------------
-- Primary Key structure for table blogs
-- ----------------------------
ALTER TABLE "public"."blogs" ADD CONSTRAINT "blogs_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table galleries
-- ----------------------------
ALTER TABLE "public"."galleries" ADD CONSTRAINT "galleries_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table migrations
-- ----------------------------
ALTER TABLE "public"."migrations" ADD CONSTRAINT "migrations_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table order_categories
-- ----------------------------
ALTER TABLE "public"."order_categories" ADD CONSTRAINT "order_categories_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table order_details
-- ----------------------------
ALTER TABLE "public"."order_details" ADD CONSTRAINT "order_details_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table orders
-- ----------------------------
ALTER TABLE "public"."orders" ADD CONSTRAINT "orders_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table partners
-- ----------------------------
ALTER TABLE "public"."partners" ADD CONSTRAINT "partners_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table password_resets
-- ----------------------------
CREATE INDEX "password_resets_email_index" ON "public"."password_resets" USING btree (
  "email" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table services
-- ----------------------------
ALTER TABLE "public"."services" ADD CONSTRAINT "services_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sliders
-- ----------------------------
ALTER TABLE "public"."sliders" ADD CONSTRAINT "sliders_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table timelines
-- ----------------------------
ALTER TABLE "public"."timelines" ADD CONSTRAINT "timelines_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_email_unique" UNIQUE ("email");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table vanue_galleries
-- ----------------------------
ALTER TABLE "public"."vanue_galleries" ADD CONSTRAINT "vanue_galleries_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table vanue_packages
-- ----------------------------
ALTER TABLE "public"."vanue_packages" ADD CONSTRAINT "vanue_packages_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table vanues
-- ----------------------------
ALTER TABLE "public"."vanues" ADD CONSTRAINT "vanues_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table areas
-- ----------------------------
ALTER TABLE "public"."areas" ADD CONSTRAINT "areas_admin_id_foreign" FOREIGN KEY ("admin_id") REFERENCES "public"."admins" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table galleries
-- ----------------------------
ALTER TABLE "public"."galleries" ADD CONSTRAINT "galleries_service_id_foreign" FOREIGN KEY ("service_id") REFERENCES "public"."services" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table order_details
-- ----------------------------
ALTER TABLE "public"."order_details" ADD CONSTRAINT "order_details_order_category_id_foreign" FOREIGN KEY ("order_category_id") REFERENCES "public"."order_categories" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table vanue_galleries
-- ----------------------------
ALTER TABLE "public"."vanue_galleries" ADD CONSTRAINT "vanue_galleries_vanue_id_foreign" FOREIGN KEY ("vanue_id") REFERENCES "public"."vanues" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table vanue_packages
-- ----------------------------
ALTER TABLE "public"."vanue_packages" ADD CONSTRAINT "vanue_packages_vanue_id_foreign" FOREIGN KEY ("vanue_id") REFERENCES "public"."vanues" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
