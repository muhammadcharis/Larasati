<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        User::create([
            'name' => 'Nessya',
            'email' => 'nessyalvioriza@gmail.com',
            'password' => Hash::make('nessya123'),
            'address' => 'Jl. Perkutut no 3',
            'phone' => '081294633942',
        ]);
    }
}
