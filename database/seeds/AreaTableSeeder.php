<?php

use Illuminate\Database\Seeder;
use App\Models\Area;
use App\Models\Admin;

class AreaTableSeeder extends Seeder
{
    public function run()
    {
        $user = Admin::where('name','Admin Larasati Blora')->first();
        $data = [
            'AREA GERBANG UTAMA',
            'AREA PENERIMA TAMU LOBY',
            'AREA PHOTOBOOTH LOBY',
            'AREA JALUR TAMU VIP & REGULER',
            'AREA PANGGUNG MUSIK',
            'AREA PELAMINAN',
            'AREA AKAD NIKAH',
            'LAIN - LAIN',
            'NOTE'
        ];

        foreach ($data as $key => $area) {
            Area::create([
                'name' => $area,
                'order_by' => $key+1,
                'is_show' => ($area == 'LAIN - LAIN' || $area == 'NOTE')? false:true,
                'admin_id' => $user->id
            ]);
        }
    }
}
