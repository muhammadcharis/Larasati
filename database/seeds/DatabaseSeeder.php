<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        Model::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('admins')->truncate();
        DB::table('users')->truncate();
        DB::table('areas')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        $this->call(AdminTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(AreaTableSeeder::class);

    }
}
