<?php

use Illuminate\Database\Seeder;
use App\Models\Admin;

class AdminTableSeeder extends Seeder
{
    public function run()
    {
        Admin::create([
            'name' => 'Admin Larasati Blora',
            'email' => 'admin@larasatiblora.com',
            'password' => Hash::make('adminlarastiblora_TMP01')
            //'password' => Hash::make('password1')
        ]);
    }
}
