<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->text('description')->nullable();
            $table->string('filename_mimetype')->nullable();
            $table->string('filename')->nullable();
            $table->boolean('is_show_on_home')->unsigned()->default(false);
            $table->boolean('is_show_on_venue')->unsigned()->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sliders');
    }
}
