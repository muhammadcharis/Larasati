<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('title');
            $table->string('slug')->unique();
            $table->text('content');
            $table->string('published_date')->nullable();
            $table->enum('status',['draft','published']);
            $table->string('featured_image')->nullable();
            $table->string('featured_mimetype')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
