<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('name');
            $table->string('email');
            $table->string('venue')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('event_name');
            $table->string('event_location');
            $table->datetime('order_date_from');
            $table->datetime('order_date_to');
            $table->char('order_number',15)->uniqe();
            $table->decimal('total_order',15,2)->default(0)->nullable();
            $table->decimal('discount',15,2)->default(0)->nullable();
            $table->decimal('total_order_after_discount',15,2)->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
