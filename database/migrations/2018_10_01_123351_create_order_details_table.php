<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('order_category_id',36);
            $table->text('detail_description')->nullable();
            $table->string('uom')->nullable();
            $table->integer('qty')->default(0);
            $table->decimal('price',15,2)->default(0)->nullable(); 
            $table->timestamps();
            $table->foreign('order_category_id')->references('id')->on('order_categories')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
