<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::prefix('/auth')->group(function(){
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('auth.login');
    Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('auth.register');
    //Route::get('/register_completed', 'Auth\LoginController@registerFinish')->name('user.registercomplete');
    Route::get('/login/{provider}', 'Auth\LoginController@redirect')->name('auth.redirect');
    Route::get('/login/{provider}/callback', 'Auth\LoginController@callback')->name('auth.callback');

    Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.show_request_form');
    Route::get('/password/reset/{id}', 'Auth\ResetPasswordController@showResetForm')->name('auth.pass_reset');

    Route::post('/login', 'Auth\LoginController@login')->name('auth.do_login');
    Route::post('/register', 'Auth\RegisterController@register')->name('auth.do_register');
    Route::post('/logout', 'Auth\LoginController@logout')->name('auth.logout');
   // Route::post('/forget_password', 'AccountController@forgetPassword')->name('user.do_forget_password');
    Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.do_pass_reset');
    Route::post('/password/reset/{id}', 'Auth\ResetPasswordController@reset')->name('auth.do_reset');

});

Route::group(['prefix' => 'admin-larasatiblora', 'namespace' => 'Admin'],function () {
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('admin.login');
    Route::get('/', 'HomeController@index')->name('admin.home');
    Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.forgot_password');
    Route::get('/password/reset/{id}/{date}/{token}', 'Auth\ResetPasswordController@showResetForm')->name('admin.resetpassword');

    Route::post('/login', 'Auth\LoginController@login')->name('admin.do_login');
    Route::post('/logout', 'Auth\LoginController@logout')->name('admin.do_logout');
    Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.sendlink');
    Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('admin.reset');
});

Route::group(['prefix' => 'admin-order', 'namespace' => 'Admin'],function () {
    Route::get('/', 'OrderController@index')->name('order.index');
    Route::get('/new', 'OrderController@create')->name('order.create');
    Route::get('/{id}/edit', 'OrderController@edit')->name('order.edit');
    
    Route::post('/store', 'OrderController@store')->name('order.store');
    Route::post('/{id}/edit', 'OrderController@update')->name('order.update');
    Route::delete('/{id}/delete', 'OrderController@destroy')->name('order.delete');
    Route::delete('/{id}/delete-order-category', 'OrderController@destroyOrderCategory')->name('order.deleteOrderCategory');
    Route::delete('/{id}/delete-order-detail', 'OrderController@destroyOrderDetail')->name('order.deleteOrderDetail');

});

Route::group(['prefix' => 'admin-slider', 'namespace' => 'Admin'],function () {
    Route::get('/', 'SliderController@index')->name('slider.index');
    Route::get('/new', 'SliderController@create')->name('slider.create');
    Route::get('/{id}/edit', 'SliderController@edit')->name('slider.edit');
    
    Route::post('/store', 'SliderController@store')->name('slider.store');
    Route::put('/{id}/edit', 'SliderController@update')->name('slider.update');
    Route::delete('/{id}/delete', 'SliderController@destroy')->name('slider.delete');

});

Route::group(['prefix' => 'admin-service', 'namespace' => 'Admin'],function () {
    Route::get('/', 'ServiceController@index')->name('service.index');
    Route::get('/new', 'ServiceController@create')->name('service.create');
    Route::get('/{id}/edit', 'ServiceController@edit')->name('service.edit');
    
    Route::post('/store', 'ServiceController@store')->name('service.store');
    Route::put('/{id}/edit', 'ServiceController@update')->name('service.update');
    Route::delete('/{id}/delete', 'ServiceController@destroy')->name('service.delete');

});

Route::group(['prefix' => 'admin-timeline', 'namespace' => 'Admin'],function () {
    Route::get('/', 'TimelineController@index')->name('timeline.index');
    Route::get('/new', 'TimelineController@create')->name('timeline.create');
    Route::get('/{id}/edit', 'TimelineController@edit')->name('timeline.edit');
    
    Route::post('/store', 'TimelineController@store')->name('timeline.store');
    Route::put('/{id}/edit', 'TimelineController@update')->name('timeline.update');
    Route::delete('/{id}/delete', 'TimelineController@destroy')->name('timeline.delete');

});

Route::group(['prefix' => 'admin-gallery', 'namespace' => 'Admin'],function () {
    Route::get('/', 'GalleryController@index')->name('gallery.index');
    Route::get('/new', 'GalleryController@create')->name('gallery.create');
    Route::get('/{id}/edit', 'GalleryController@edit')->name('gallery.edit');
    
    Route::post('/store', 'GalleryController@store')->name('gallery.store');
    Route::put('/{id}/edit', 'GalleryController@update')->name('gallery.update');
    Route::delete('/{id}/delete', 'GalleryController@destroy')->name('gallery.delete');

});

Route::group(['prefix' => 'admin-venue', 'namespace' => 'Admin'],function () {
    Route::get('/', 'VenueController@index')->name('venue.index');
    Route::get('/new', 'VenueController@create')->name('venue.create');
    Route::get('/{id}/edit', 'VenueController@edit')->name('venue.edit');
    
    Route::post('/store', 'VenueController@store')->name('venue.store');
    Route::put('/{id}/edit', 'VenueController@update')->name('venue.update');
    Route::delete('/{id}/delete', 'VenueController@destroy')->name('venue.delete');

});

Route::group(['prefix' => 'admin-blog', 'namespace' => 'Admin'],function () {
    Route::get('/', 'BlogController@index')->name('blog.index');
    Route::get('/new', 'BlogController@create')->name('blog.create');
    Route::get('/{id}/edit', 'BlogController@edit')->name('blog.edit');
    
    Route::post('/store', 'BlogController@store')->name('blog.store');
    Route::put('/{id}/edit', 'BlogController@update')->name('blog.update');
    Route::delete('/{id}/delete', 'BlogController@destroy')->name('blog.delete');

});

Route::group(['prefix' => 'admin-partner', 'namespace' => 'Admin'],function () {
    Route::get('/', 'PartnerController@index')->name('partner.index');
    Route::get('/new', 'PartnerController@create')->name('partner.create');
    Route::get('/{id}/edit', 'PartnerController@edit')->name('partner.edit');
    
    Route::post('/store', 'PartnerController@store')->name('partner.store');
    Route::put('/{id}/edit', 'PartnerController@update')->name('partner.update');
    Route::delete('/{id}/delete', 'PartnerController@destroy')->name('partner.delete');

});



//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');


/*
|--------------------------------------------------------------------------
|  Haram Web Routes
|--------------------------------------------------------------------------
| Please add controller, and then place function on that controller. ;)
|
*/



Route::get('/', 'FrontController@index')->name('index');
Route::get('/tentang-kami', 'FrontController@about')->name('about');
Route::get('/galeri', 'FrontController@gallery')->name('gallery');
Route::get('/gedung', 'FrontController@venue')->name('venue');

Route::post('/appointment/post', 'FrontController@appoitmentPost')->name('appoitmentPost');

Route::get('/slider/{filename}', 'FrontController@sliderImage')->name('sliderImage');
Route::get('/about-us/{filename}', 'FrontController@aboutUsImage')->name('aboutImage');
Route::get('/service/{filename}', 'FrontController@serviceImage')->name('serviceImage');
Route::get('/gallery/{filename}', 'FrontController@galleryImage')->name('galleryImage');
Route::get('/gallery/{filename}/thumbnail', 'FrontController@galleryImageTumbnaile')->name('galleryImageTumbnail');

Route::get('/get-services-data', 'FrontController@serviceData');
Route::get('/get-services-detail', 'FrontController@serviceDetail');
Route::get('/get-slider-home-data', 'FrontController@sliderHomeData');
Route::get('/get-about-us-data', 'FrontController@aboutUsData');
Route::get('/get-gallery-data', 'FrontController@galleryData');
Route::get('/get-venue-agenda', 'FrontController@venueAgenda');
Route::get('/get-venue-package-data', 'FrontController@venuePackageData');
Route::get('/get-slider-venue-data', 'FrontController@sliderVenueData');
Route::get('/get-instagram-feed', 'FrontController@instagramFeed');

Route::get('/logo', 'FrontController@logo')->name('logo');

Route::get('/blog', function () {
    return view('blog');
})->name('blog');

Route::get('/blog/{slug}', function () {
    return view('blog_detail');
})->name('blog.detail');

Route::get('/kontak', function () {
    return view('contact');
})->name('contact');

Route::get('/instagram/callback', 'FrontController@intagram')->name('api.instagram');

Route::get('/email', function () {
    return view('email');
})->name('email');

Route::get('/static', function () {
    return view('static');
})->name('static');
