let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
	.scripts([
        'resources/assets/js/pace.min.js',
    	'resources/assets/js/limitless/libraries/jquery.min.js',
    	'resources/assets/js/limitless/libraries/bootstrap.min.js',
    	'resources/assets/js/limitless/app.min.js',
    	'resources/assets/js/nicescroll.min.js',
    	'resources/assets/js/drilldown.js',
        'resources/assets/vendor/moment/moment.min.js',
        'resources/assets/js/select2.min.js',
        'resources/assets/js/jgrowl.min.js',
        'resources/assets/js/data-tables/datatables.min.js',
        'resources/assets/js/blockui.min.js'
    ],'public/js/backend.js')
    .scripts('resources/assets/js/mustache.js', 'public/js/mustache.js')
    .scripts('resources/assets/js/formatMoney.js', 'public/js/formatMoney.js')
    .scripts([
        'resources/assets/vendor/pickadate/picker.js',
        'resources/assets/vendor/pickadate/picker.date.js',
        'resources/assets/vendor/pickadate/picker.time.js',
        'resources/assets/vendor/pickadate/legacy.js',
    ],'public/js/pickadate.js')
    .scripts([
        'resources/assets/js/admin/order.js',
    ],'public/js/admin/order.js')
    .scripts([
        'resources/assets/js/admin/venue.js',
    ],'public/js/admin/venue.js')
    .scripts([
        'resources/assets/js/datepicker.js',
        'resources/assets/js/daterangepicker.js',
    ],'public/js/datepicker.js')
    .scripts([
        'resources/assets/js/plugins/notifications/sweet_alert.min.js',
        'resources/assets/js/pages/notification.js',
    ],'public/js/notifications.js')
    .scripts([
        "resources/assets/js/plugins/notifications/bootbox.min.js"
    ],"public/js/bootbox.js")
    .scripts([
        'resources/assets/js/costum.js',
        'resources/assets/js/fontawesome-all.min.js',
        'resources/assets/js/nod.js',
    ],'public/js/costum.js')
    .scripts([
        'resources/assets/js/jgrowl.min.js',
    ],'public/js/jgrowl.js')
    .scripts('resources/assets/js/jquery.waypoints.min.js', 'public/js/jquery.waypoints.min.js')
    .scripts('resources/assets/js/about.js', 'public/js/about.js')
    .scripts([
        'resources/assets/vendor/fullcalendar-3.9.0/lib/moment.min.js',
        'resources/assets/vendor/fullcalendar-3.9.0/fullcalendar.min.js',
    ],'public/js/fullcalendar.js')
    .scripts([
        'resources/assets/js/masonry.pkgd.min.js',
    ],'public/js/masonry.pkgd.min.js')
    .scripts([
        'resources/assets/js/imagesloaded.pkgd.min.js',
    ],'public/js/imagesloaded.pkgd.min.js')
    .scripts([
        'resources/assets/vendor/lightbox/js/lightbox.min.js',
    ],'public/js/lightbox.min.js')
    .scripts([
        'resources/assets/js/owl.carousel.min.js',
    ], 'public/js/owl-carousel.min.js')

   	.styles([
        'resources/assets/css/limitless/icons/icomoon/styles.css',
        'resources/assets/css/limitless/bootstrap.min.css',
        'resources/assets/css/limitless/core.css',
        'resources/assets/css/limitless/components.min.css',
        'resources/assets/css/limitless/colors.min.css',
        'resources/assets/css/limitless/style.css',
    ],'public/css/backend.css')
    .styles([
        'resources/assets/css/costum.css',
        'resources/assets/css/responsive.css',
        'resources/assets/css/fa-svg-with-js.css',
    ],'public/css/costum.css')
    .styles([
        'resources/assets/css/owl.carousel.min.css',
        'resources/assets/css/owl.theme.default.min.css'
    ],'public/css/owl-carousel.min.css')
    .styles([
        'resources/assets/css/bootstrap-datepicker3.min.css',
    ],'public/css/datepicker.min.css')
    .styles('resources/assets/css/animate.css','public/css/animate.css')
    .styles([
        'resources/assets/vendor/fullcalendar-3.9.0/fullcalendar.min.css',
    ],'public/css/fullcalendar.css')
    .styles([
        'resources/assets/vendor/fullcalendar-3.9.0/fullcalendar.print.min.css',
    ],'public/css/fullcalendar.print.css')
    .styles([
        'resources/assets/vendor/lightbox/css/lightbox.min.css',
    ],'public/css/lightbox.min.css')



   	.copyDirectory('resources/assets/fonts/icomoon','public/fonts/icomoon')
   	.copyDirectory('resources/assets/images', 'public/images')
    .copyDirectory('resources/assets/vendor/summernote','public/vendor/summernote')
    .copyDirectory('resources/assets/vendor/datatables','public/vendor/datatables')
    .copyDirectory('resources/assets/vendor/lightbox/images','public/images')





    .version();
