<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## About Larasati

Larasati is a wedding decorator based on blora, to grow up our business we are using web application to promote other customer from outside blora.
We are using laravel framework as our core system. ( thanks to Taylor Otwell and team for an amazing framework)

## Larasati Sponsors

We would like to extend our thanks to the following sponsors for helping fund on-going Larasati Web Application.:

- **[Laravel Framework](http://laravel.com)**
- **[Stack Overflow](http://stackoverflow.com)**
