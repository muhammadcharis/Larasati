<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class SessionId
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Session::has("session_id")){
            $sessionId = openssl_random_pseudo_bytes(8);
            $sessionId = base64_encode($sessionId);
            Session::put("session_id", $sessionId);
            Session::save();
        }
        return $next($request);
    }
}
