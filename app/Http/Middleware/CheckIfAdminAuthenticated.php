<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckIfAdminAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $auth = Auth::guard('admins');
        
        if (!$auth->check()) {
            return redirect('/admin-larasatiblora/login');
        }

        
        if (Auth::guard('admins')->user()->deleted_at != null) {
            Auth::guard('admins')->logout();

            return redirect()->action('Admin\Auth\LoginController@showLoginForm')
                    ->with('msg', 'This Account has Banned');
        }

        return $next($request);
    }
}
