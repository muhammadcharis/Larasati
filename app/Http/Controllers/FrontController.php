<?php namespace App\Http\Controllers;

use Config;
use StdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Vinkla\Instagram\Instagram;


use App\Models\Slider;
use App\Models\Service;
use App\Models\Gallery;
use App\Models\Timeline;
use App\Models\Appointment;
use App\Models\OrderCategory;
use App\Models\Venue;


class FrontController extends Controller
{
    public function index(){
        return view('index');
    }

    static function serviceData(){
        $services = Service::orderby('created_at','asc')->get();
        $array = array();

        foreach ($services as $key => $service) {
            $obj = new stdClass();
            $obj->slug = $service->slug;
            $obj->name = ucwords($service->name);
            $obj->description = $service->description;
            $obj->service_image = route('serviceImage',$service->filename);
            $obj->url_go_to_gallery = route('gallery').'#'.$service->slug;
            $array [] = $obj;
        }

        return response()->json(['services' => $array]);

    }

    static function serviceImage($filename){
        $path = Config::get('storage.service');
        $resp = response()->download($path . '/'. $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    static function sliderHomeData(){
        $sliders = Slider::orderby('created_at','desc')
        ->where('is_show_on_home',true)
        ->limit(3)
        ->get();

        $array = array();

        foreach ($sliders as $key => $slider) {
            $obj = new stdClass();
            $obj->active = ($key == 0)? 'active' : null;
            $obj->description = $slider->description;
            $obj->slider_image = route('sliderImage',$slider->filename);
            $array [] = $obj;
        }

        return response()->json(['sliders' => $array]);

    }

    static function sliderVenueData(){
        $sliders = Slider::orderby('created_at','desc')
        ->where('is_show_on_venue',true)
        ->limit(3)
        ->get();

        $array = array();

        foreach ($sliders as $key => $slider) {
            $obj = new stdClass();
            $obj->active = ($key == 0)? 'active' : null;
            $obj->description = $slider->description;
            $obj->slider_image = route('sliderImage',$slider->filename);
            $array [] = $obj;
        }

        return response()->json(['sliders' => $array]);

    }

    static function sliderImage($filename){
        $path = Config::get('storage.slider');
        $resp = response()->download($path . '/'. $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function about(){
        $timelines = Timeline::orderby('timeline_date','desc')->get();
        return view('about',compact('timelines'));
    }

    static function aboutUsData(){
        
        $timelines = Timeline::orderby('timeline_date','desc')->get();
        $array = array();

        foreach ($timelines as $key => $timeline) {
            $obj = new stdClass();
            $obj->description = $timeline->description;
            $obj->timeline_date = substr($timeline->timeline_date->format('Y'),0,2);
            $obj->timeline_year = $timeline->timeline_date->format('y');
            $obj->timeline_image = route('aboutImage',$timeline->filename);
            $array [] = $obj;
        }

        return response()->json(['timelines' => $array]);
    }

    static function aboutUsImage($filename){
        $path = Config::get('storage.timeline');
        $resp = response()->download($path . '/'. $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function gallery(){
        $galleries = $this->galleryData();
        //return response()->json($galleries);
        return view('gallery',compact('galleries'));
    }

    static function galleryData(){
        $galleries = Gallery::with('service')->orderby('created_at','asc')->get();
        $services = Service::orderby('created_at','asc')->get();
        $array_gallery = array();
        $array_service = array();

        $obj_0 = new StdClass();
        $obj_0->slug = 'all';
        $obj_0->service_name = 'All Service';
        $obj_0->description = 'All service';
        $obj_0->aria_controls = 'all';

        foreach ($galleries as $key => $gallery) {
            $obj_detail_0 = new stdClass();
            $obj_detail_0->service_name = $gallery->service->name;
            $obj_detail_0->title = $gallery->name;
            $obj_detail_0->description = $gallery->description;
            $obj_detail_0->gallery_image_tumbnail = route('galleryImageTumbnail',$gallery->filename);
            $obj_detail_0->gallery_image = route('galleryImage',$gallery->filename);
            $array_gallery [] = $obj_detail_0;
        }
        $obj_0->details = $array_gallery;
        $array_service [] = $obj_0;

        foreach ($services as $key => $service) {
            $obj = new StdClass();
            $obj->slug = $service->slug;
            $obj->service_name = ucwords($service->name);
            $obj->description = $service->description;
            $obj->aria_controls = ($key+1);

            $service_galleries = $service->galleries()->get();
            $array_detail = array();
            foreach ($service_galleries as $key_1 => $service_gallery) {
                $obj_detail = new StdClass();
                $obj_detail->service_name = $gallery->service->name;
                $obj_detail->title = $service_gallery->title;
                $obj_detail->description = $service_gallery->description;
                $obj_detail->gallery_image_tumbnail = route('galleryImageTumbnail',$service_gallery->filename);
                $obj_detail->gallery_image = route('galleryImage',$service_gallery->filename);
                $array_detail [] = $obj_detail;
            }

            $obj->details = $array_detail;
            $array_service [] = $obj;
        }

        /*return response()->json([
            'galleries' => $array_service,
        ],200);*/
        return $array_service;
    }

    
    public function galleryImageTumbnaile($filename){
        $path = Config::get('storage.gallery_thumbnaile');

        $resp = response()->download($path . '/'. $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');

        return $resp;
    }

    public function galleryImage($filename){
        $path = Config::get('storage.gallery');

        $resp = response()->download($path . '/'. $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');

        return $resp;
    }

    public function venue(){
        return view('venue');
    }

    static function venuePackageData(){
        $venues = Venue::orderby('created_at','asc')->get();

        $array = array();
        foreach ($venues as $key => $venue) {
            $venue_packages = $venue->venuePackage()->get();

            $obj = new stdClass();
            $obj->package_name = $venue->name;

            $details = array();
            foreach ($venue_packages as $key_1 => $venue_package) {
                $obj_details = new StdCLass();
                $obj_details->description = $venue_package->description;
                $details [] = $obj_details;
            }

            $obj->details = $details;
            $array [] = $obj;
        }

        return response()->json([
            'packages' => $array,
        ],200);
    }

    public function appoitmentPost(Request $request){
        $appoitment = Appointment::create([
            'name' => ucwords($request->name),
            'email' => $request->email,
            'no_tlpn' => $request->phone_number,
            'date_appointment' => $request->date,
            'appointment_type' => $request->appointment_type,
            'description' => $request->description,
        ]);
        
    }

    public function intagram(Request $request){
        $code = $request->code;
        $uri = "https://api.instagram.com/oauth/access_token";
        $client_id = 'd4d99e2759104825999fe372b378aada';
        $client_secret = 'b5f3b8f9d41643cbaa725577514e1f43';
        $redirect_uri = 'http://larasatiblora.com/instagram/callback';
        $access_token_parameters = array(
            'client_id'                =>     $client_id,
            'client_secret'            =>     $client_secret,
            'grant_type'               =>     'authorization_code',
            'redirect_uri'             =>     $redirect_uri,
            'code'                     =>     $code
        );

       /*

        curl -F 'client_id=d4d99e2759104825999fe372b378aada' \
        -F 'client_secret=b5f3b8f9d41643cbaa725577514e1f43' \
        -F 'grant_type=authorization_code' \
        -F 'redirect_uri=http://larasatiblora.com/instagram/callback' \
        -F 'code=5e9118d69ef24db1bd8146e9b1a19aae' \
        https://api.instagram.com/oauth/access_token

       */
      
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $uri); // uri
        curl_setopt($curl, CURLOPT_POST, true); // POST
        curl_setopt($curl, CURLOPT_POSTFIELDS, $access_token_parameters); // POST DATA
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // RETURN RESULT true
        curl_setopt($curl, CURLOPT_HEADER, 0); // RETURN HEADER false
        curl_setopt($curl, CURLOPT_NOBODY, 0); // NO RETURN BODY false / we need the body to return
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0); // VERIFY SSL HOST false
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // VERIFY SSL PEER false
  
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        echo '<pre>'; // preformatted view
        
        if ($err) return "cURL Error #:" . $err;
        else{
            $return = json_decode($response,true);
            $access_token = $return->access_token;
            $this->setEnv('AUTH_IG',$access_token);
            print_r($return); 
        }

    }

    static function setEnv($name, $value){
        $path = base_path('.env');
        if (file_exists($path)) {
            file_put_contents($path, str_replace(
                $name . '=' . env($name), $name . '=' . $value, file_get_contents($path)
            ));
        }
    }

    static function instagramFeed(){
        $access_token = env('AUTH_IG','d4d99e2759104825999fe372b378aada');
        $instagram = new Instagram($access_token);//c07aa7c2b1984a38a71bcab0b3f14ed0
        $instagrams = $instagram->get();

        return response()->json([
            'instagrams' => $instagrams,
        ],200);
    }

    public function logo(Request $request){
        $path = base_path().'/public/images/icon_larasati.png';
        $resp = response()->download($path);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;

	}
	
}
