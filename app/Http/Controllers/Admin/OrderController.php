<?php namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session; 

//models
use App\Models\Order;
use App\Models\OrderCategory;
use App\Models\OrderDetail;


class OrderController extends Controller
{
    public function index(Request $request, Builder $htmlBuilder){
        if ($request->ajax()){ 
            $orders = Order::orderBy('created_at','asc');

            return DataTables::of($orders)
            ->editColumn('order_date_from',function ($orders){
                return $orders->order_date_from->format('d/m/Y h:i A');
            })
            ->editColumn('order_date_to',function ($orders){
                return $orders->order_date_to->format('d/m/Y h:i A');
            })
            ->editColumn('total_order',function ($orders){
                return number_format($orders->total_order,2,',','.');
            })
            ->addColumn('action', function($orders) {
                return view('_action', [
                    'model' => $orders,
                    'edit' => route('order.edit',$orders->id),
                    'delete' => route('order.delete',$orders->id),
                ]);
            })     
            ->make(true);
        }

        $html = $htmlBuilder
        ->addColumn(['data'=>'order_number', 'name'=>'order_number', 'title'=>'Order Number'])
        ->addColumn(['data'=>'name', 'name'=>'name', 'title'=>'Cust. Name'])
        ->addColumn(['data'=>'email', 'name'=>'email', 'title'=>'Cust. Email'])
        ->addColumn(['data'=>'event_name', 'name'=>'event_name', 'title'=>'Event Name'])
        ->addColumn(['data'=>'event_location', 'name'=>'event_location', 'title'=>'Location'])
        ->addColumn(['data'=>'order_date_from', 'name'=>'order_date', 'title'=>'Order Date Start'])
        ->addColumn(['data'=>'order_date_to', 'name'=>'order_date', 'title'=>'Order Date End'])
        ->addColumn(['data'=>'total_order', 'name'=>'total_order', 'title'=>'Total Order'])
        ->addColumn(['data' => 'action','name' => 'action', 'title' => 'ACTION', 'orderable' => false, 'searchable' => false, 'class' => 'text-center', 'style'=>'width: 30px;']);
        
        $flag = Session::get('flag');
        return view('admin.order.index',compact('html','flag'));
    }

    public function create(){
        return view('admin.order.create');
    }

    public function store(Request $request){

        if(Session::has('flag')) Session::forget('flag');

        $customer_name = ucwords($request->customer_name);
        $customer_email = $request->customer_email;
        $customer_phone_number = $request->customer_phone_number;
        $event_name = ucwords($request->event_name);
        $event_location = ucwords($request->event_location);
        $total_price_all_category = $request->total_price_all_category;
        $event_date = explode('-',$request->event_date);
        $start_event  = Carbon::createFromFormat('d/m/Y h:i A', trim($event_date[0]))->format('Y-m-d H:i:s'); 
        $end_event  =  Carbon::createFromFormat('d/m/Y h:i A', trim($event_date[1]))->format('Y-m-d H:i:s');
        $order_categories = json_decode($request->order_details);
        
        $order = Order::FirstOrCreate([
            'order_number' => Order::createOrderNumber(),
            'name' => $customer_name,
            'email' => $customer_email,
            'phone_number' => $customer_phone_number,
            'event_name' => $event_name,
            'event_location' => $event_location,
            'order_date_from' => $start_event,
            'order_date_to' => $end_event,
            'total_order' => $total_price_all_category,
        ]);
       
        $this->insert_order_details($order_categories,$order->id);
        Session::flash('flag', 'success');
        return response()->json(200);
    }

    public function edit($id){
        $order = Order::find($id);
        $order_categories = OrderCategory::where('order_id',$id)->get();
        $array = array();

        foreach ($order_categories as $key => $order_category) {
            $obj = new stdClass();
            $order_category_id = $order_category->id;
            $category = $order_category->name;
            $qty_kategori = $order_category->qty;
            $uom_kategori = $order_category->uom;
            $total_price = FloatVal($order_category->total_per_category);
            $total_price_format_money = number_format($total_price,2,',','.');
            $order_details = $order_category->orderDetail()->get();

            if ($category == 'DEKORASI' || $category == 'RIAS' || $category == 'WEDDING_ORGANIZER' ||  $category == 'SEWA_GEDUNG') $flag_set = true;
            else $flag_set = false;
            

            $obj->id = $order_category_id;
            $obj->url_delete_order_category = route('order.deleteOrderCategory',$order_category_id);
            $obj->category = $category;
            $obj->qty_kategori = $qty_kategori;
            $obj->uom_kategori = $uom_kategori;
            $obj->total_price = $total_price;
            $obj->total_price_format_money = $total_price_format_money;
            $obj->flag_set = $flag_set;
            
            $details = array();
            foreach ($order_details as $key_2 => $order_detail) {
                $order_detail_id = $order_detail->id;
                $detail_order = $order_detail->detail_description;
                $uom_detail = $order_detail->uom;
                $qty_detail = FloatVal($order_detail->qty);
                $price_detail = FloatVal($order_detail->price);
                $total_price_qty = $qty_detail * $price_detail;

                $obj_detail = new stdClass();
                $obj_detail->id = $order_detail_id;
                $obj_detail->url_delete_order_detail = route('order.deleteOrderDetail',$order_detail_id);
                $obj_detail->detail_order = $detail_order;
                $obj_detail->uom = $uom_detail;
                $obj_detail->qty = $qty_detail;
                $obj_detail->harga_satuan = $price_detail;
                $obj_detail->total_price_qty = $total_price_qty;
                $obj_detail->harga_satuan_format_money = number_format($price_detail,2,',','.');
                $obj_detail->total_price_qty_format_money = number_format($total_price_qty,2,',','.');
                $details [] = $obj_detail;
            }

            $obj->details = $details;
            $array [] = $obj;
        }
        
        return view('admin.order.edit',compact('order','array'));
    }

    public function update(Request $request,$id){
        if(Session::has('flag')) Session::forget('flag');

        $customer_name = ucwords($request->customer_name);
        $customer_email = $request->customer_email;
        $customer_phone_number = $request->customer_phone_number;
        $event_name = ucwords($request->event_name);
        $event_location = ucwords($request->event_location);
        $total_price_all_category = $request->total_price_all_category;
        $event_date = explode('-',$request->event_date);
        $start_event  = Carbon::createFromFormat('d/m/Y h:i A', trim($event_date[0]))->format('Y-m-d H:i:s'); 
        $end_event  =  Carbon::createFromFormat('d/m/Y h:i A', trim($event_date[1]))->format('Y-m-d H:i:s');
        $order_categories = json_decode($request->order_details);

        $order = Order::find($id);
        $order->name = $customer_name;
        $order->email = $customer_email;
        $order->phone_number = $customer_phone_number;
        $order->event_name = $event_name;
        $order->event_location = $event_location;
        $order->order_date_from = $start_event;
        $order->order_date_to = $end_event;
        $order->total_order = $total_price_all_category;
        $order->save();
        
        
        $this->insert_order_details($order_categories,$id);
        Session::flash('flag', 'success_2');
        return response()->json(200);
    }

    static function insert_order_details($order_categories,$order_id){
        foreach ($order_categories as $key => $order_category) {
            $order_category_id = $order_category->id;
            $category = $order_category->category;
            $qty_category = $order_category->qty_kategori;
            $uom_category = isset($order_category->uom_kategori)? $order_category->uom_kategori : null;
            $total_price_per_category = $order_category->total_price;
            $order_details = $order_category->details;

            if($order_category_id == -1){
                $order_category = OrderCategory::FirstOrCreate([
                    'order_id' => $order_id,
                    'name' => $category,
                    'uom' => $uom_category,
                    'qty' => $qty_category,
                    'total_per_category' => $total_price_per_category
                ]);
            }else{
                $order_category = OrderCategory::find($order_category_id);
                $order_category->name = $category;
                $order_category->uom = $uom_category;
                $order_category->qty = $qty_category;
                $order_category->total_per_category = $total_price_per_category;
                $order_category->save();
            }
            
            
            foreach ($order_details as $key_2 => $order_detail) {
                $order_detail_id = $order_detail->id;
                $description = ucwords($order_detail->detail_order);
                $uom_detail = isset($order_detail->uom)? $order_detail->uom : null;
                $qty_detail = isset($order_detail->qty)? $order_detail->qty : null;
                $harga_satuan = isset($order_detail->harga_satuan)? $order_detail->harga_satuan : null;
                $total_price_qty = isset($order_detail->total_price_qty)? $order_detail->total_price_qty : null;

                if($order_detail_id == -1){
                    $order_detail = OrderDetail::FirstOrCreate([
                        'order_category_id' => $order_category->id,
                        'detail_description' => $description,
                        'uom' => $uom_detail,
                        'qty' => $qty_detail,
                        'price' => $total_price_qty
                    ]);
                }else{
                    $order_detail = OrderDetail::find($order_detail_id);
                    $order_detail->detail_description = $description;
                    $order_detail->uom = $uom_detail;
                    $order_detail->qty = $qty_detail;
                    $order_detail->price = $total_price_qty;
                    $order_detail->save();
                }
                
            }

        }
    }

    public function destroy($id){
        Order::findorFail($id)->delete();
        return response()->json(200);
    }

    public function destroyOrderCategory($id){
        $order_category = OrderCategory::findorFail($id);
        $order_id = $order_category->order_id;
        $total_per_category = $order_category->total_per_category;

        $order = Order::find($order_id);
        $total_order = $order->total_order;
        $new_total_order = $total_order - $total_per_category;
        
        $order->total_order = $new_total_order;
        $order->save();

        $order_category->delete();
        return response()->json(200);
    }

    public function destroyOrderDetail($id){
        $order_detail = OrderDetail::findorFail($id);
        $order_category_id  = $order_detail->order_category_id;
        $qty  = $order_detail->qty;
        $price  = $order_detail->price;
        $total_detail_price = $qty * $price;

        if($total_detail_price){
            $order_category = OrderCategory::find($order_category_id);
            $order_id = $order_category->order_id;
            $total_per_category = $order_category->total_per_category;

            $new_total_per_category = $total_per_category - $total_detail_price;
            $order_category->total_per_category = $new_total_per_category;
            $order_category->save();

            $order = Order::find($order_id);
            $order->total_order = OrderCategory::where('oder_id',$order_id)->sum('total_per_category');
            $order->save();
        }
        
        
        $order_detail->delete();

        return response()->json(200);
    }
}
