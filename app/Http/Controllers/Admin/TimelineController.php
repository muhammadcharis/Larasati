<?php namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use File;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\Timeline;


class TimelineController extends Controller
{
    public function index(Request $request, Builder $htmlBuilder){
        if ($request->ajax()) { 
            $timelines = Timeline::orderby('timeline_date','desc');

            return DataTables::of($timelines)
            ->editColumn('timeline_date',function($timelines){
                return $timelines->timeline_date->format('d/M/Y');
            })
            ->addColumn('action', function($timelines){ return view('includes.backend._action', [
                'model' => $timelines,
                'edit_modal' => route('timeline.edit', $timelines->id),
                'delete' => route('timeline.delete', $timelines->id)
                ]);
            })    
            ->make(true);
        }

        $html = $htmlBuilder
        ->addColumn(['data'=>'timeline_date', 'name'=>'timeline_date', 'title'=>'Timeline Date'])
        ->addColumn(['data'=>'description', 'name'=>'description', 'title'=>'Description'])
        ->addColumn(['data' => 'action','name' => 'action', 'title' => 'Action', 'orderable' => false, 'searchable' => false, 'class' => 'text-center']);
        
        return view('admin.timeline.index',compact('html'));
    }
    
    public function store(Request $request){
        $timeline_path = Config::get('storage.timeline');
        if (!File::exists($timeline_path)) File::makeDirectory($timeline_path, 0777, true);
        
        $image = $request->file('file_upload');
        $random = Timeline::random($image);
        $_random = $random[1];
        $image_mimetype = $image->getMimeType(); 
        $image_resize = Image::make($image->getRealPath());
        $width = $image_resize->width();
        $height = $image_resize->height();
        
        if($width > 1920 && $height >1080)
            $image_resize->resize(1920, 1080);

        
        $timeline = Timeline::create([
            'timeline_date' => Carbon::createFromFormat('d/m/Y', $request->timeline_date),
            'description' => $request->description,
            'filename' => $_random,
            'filename_mimetype' => $image_mimetype
        ]);

        if ($timeline->save() && $image)
            $image_resize->save($random[0].'/'.$random[1]);
        
        return response()->json(200);
    }
    
    public function edit($id){
        //
    }

    public function update(Request $request, $id){
        //
    }
    
    public function destroy($id){
        $timeline = Timeline::findOrFail($id);
        $image = $timeline->getFullPath();
        if ($timeline->delete() && File::exists($image))
            @unlink($image);
    }
}
