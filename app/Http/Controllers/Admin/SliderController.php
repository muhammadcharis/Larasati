<?php namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use File;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;


use App\Models\Slider;

class SliderController extends Controller
{
    public function __construct(){
        $this->middleware('auth.admin');
    }

    public function index(Request $request, Builder $htmlBuilder){
        if ($request->ajax()) { 
            $sliders = Slider::orderby('created_at','asc');

            return DataTables::of($sliders)
            ->editColumn('is_show_on_home',function ($sliders){
                if($sliders->is_show_on_home) return '<i class="icon-checkmark4"></i>';
                else 'false';
            })
            ->editColumn('is_show_on_venue',function ($sliders){
                if($sliders->is_show_on_venue) return '<i class="icon-checkmark4"></i>';
                else 'false';
            })
            ->addColumn('action', function($sliders){ return view('includes.backend._action', [
                'model' => $sliders,
                'edit_modal' => route('slider.edit', $sliders->id),
                'delete' => route('slider.delete', $sliders->id)
                ]);
            })    
            ->rawColumns(['is_show_on_venue','is_show_on_home','action'])
            ->make(true);
        }

        $html = $htmlBuilder
        ->addColumn(['data'=>'description', 'name'=>'description', 'title'=>'Description'])
        ->addColumn(['data'=>'is_show_on_home', 'name'=>'description', 'title'=>'Show On Home'])
        ->addColumn(['data'=>'is_show_on_venue', 'name'=>'description', 'title'=>'Show On Venue'])
        ->addColumn(['data' => 'action','name' => 'action', 'title' => 'Action', 'orderable' => false, 'searchable' => false, 'class' => 'text-center']);
        
        return view('admin.slider.index',compact('html'));
    }

    public function store(Request $request){
        $slider_path = Config::get('storage.slider');
        if (!File::exists($slider_path)) File::makeDirectory($slider_path, 0777, true);
        
        $validator = Validator::make($request->all(), [
            'file_upload' => 'required',
        ]);

        if ($validator->passes()){
            $image = $request->file('file_upload');
            $random = Slider::random($image);
            $_random = $random[1];
            $image_mimetype = $image->getMimeType(); 
            $image_resize = Image::make($image->getRealPath());
            $width = $image_resize->width();
            $height = $image_resize->height();
            
            if($width > 1920 && $height > 1080)
            $image_resize->resize(1920, 1080);

            $show_on = $request->show_on;

            $is_show_on_home = false;
            $is_show_on_venue = false;

            if($show_on == 'booth'){
                $is_show_on_home = true;
                $is_show_on_venue = true;
            }

            if($show_on == 'home') $is_show_on_home = true;
            if($show_on == 'venue') $is_show_on_venue = true;

            

            $slider = Slider::create([
                'description' => $request->description,
                'filename' => $_random,
                'filename_mimetype' => $image_mimetype,
                'is_show_on_home' => $is_show_on_home,
                'is_show_on_venue' => $is_show_on_venue,
            ]);

            if ($slider->save() && $image)
                $image_resize->save($random[0].'/'.$random[1]);
            
            return response()->json(200);
        }else{
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ],400);
        }
        
        
    }

    public function edit($id){
        //
    }

    public function update(Request $request, $id){
        //
    }

    public function destroy($id){
        $slider = Slider::findOrFail($id);
        $image = $slider->getFullPath();
        if ($slider->delete() && File::exists($image))
            @unlink($image);
    }
}
