<?php namespace App\Http\Controllers\Admin;

use Auth;
use Validator;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

//models
use App\Models\Venue;
use App\Models\VenuePackage;

class VenueController extends Controller
{
    public function index(Request $request, Builder $htmlBuilder){
         if ($request->ajax()){ 
            $venues = Venue::orderBy('created_at','desc');

            return DataTables::of($venues)
             ->addColumn('detail', function($venues) {
                $venue_packages = VenuePackage::where('venue_id',$venues->id)->get();
                $_temp = '';
                foreach ($venue_packages as $key => $venue_package) {
                    $_temp .= $venue_package->description.',';
                }

                return substr($_temp,0,-1);
            })  
            ->addColumn('action', function($venues) {
                return view('_action', [
                    'model' => $venues,
                    'edit' => route('venue.edit',$venues->id),
                    'delete' => route('venue.delete',$venues->id),
                ]);
            })     
            ->make(true);
        }

        $html = $htmlBuilder
        ->addColumn(['data'=>'name', 'name'=>'name', 'title'=>'Package Name'])
        ->addColumn(['data' => 'detail','name' => 'detail', 'title' => 'Package Detail', 'orderable' => false, 'searchable' => false])
        ->addColumn(['data' => 'action','name' => 'action', 'title' => 'ACTION', 'orderable' => false, 'searchable' => false, 'class' => 'text-center', 'style'=>'width: 30px;']);
        
        $flag = Session::get('flag');
        return view('admin.venue.index',compact('html','flag'));
    }

    public function store(Request $request){
        $package_name = ucwords($request->name);
        $venue_packages = json_decode($request->venue_packages);

        $venue = Venue::FirstOrCreate([
            'name' => $package_name
        ]);

        foreach ($venue_packages as $key => $venue_package) {
            $venue_detail_id = $venue_package->venue_detail_id;
            $description = trim(ucfirst($venue_package->description));

            if($venue_detail_id == -1){
                $order_category = VenuePackage::FirstOrCreate([
                    'venue_id' => $venue->id,
                    'description' => $description,
                ]);
            }else{
                $_venue_package = VenuePackage::find($venue_detail_id);
                $_venue_package->description = $description;
                $_venue_package->save();
            }

        }
        return response()->json(200);
    }

    public function edit($id)
    {
        
    }

    public function destroy($id)
    {
        //
    }
}
