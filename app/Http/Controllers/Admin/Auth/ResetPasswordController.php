<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Http\Requests\Customer\ResetPasswordRequest;
use Auth;
use Hash;

use App\Models\Admin;
use Config;
use Carbon\Carbon;
use Mail;
class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $redirectTo = '/admin';

    public function __construct()
    {
        $this->middleware('guest.admin');
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  string|null $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm($id, $date, $token)
    {
        $admin = Admin::find($id);
        if (!$admin)
            return redirect()->action('AuthController@loginRegister')
                ->with('_ACCOUNT.RESET_FAIL', true);

        $datetime = Carbon::createFromFormat('YmdHi', $date);
        $dreset = Carbon::now()->subHours(1);

        $hash_token = base64_encode(hash_hmac('sha256', $date|$admin->email, Config::get('app.reminder_key'), true));
        $hash_token = strtr($hash_token, ['/' => '_', '+' => '-', '=' => '~']);

        if ($datetime < $dreset || $token != $hash_token)
            return redirect()->action('AuthController@loginRegister')
                ->with('_ACCOUNT.RESET_FAIL', true);

        return view('auth.passwords.reset')->with(
            ['token' => $token, 'admin' => $admin, 'date' => $date]
        );
    }

    public function reset(ResetPasswordRequest $request){
        $id = $request->id;
        $date = $request->date;
        $token = $request->token;
        $email = $request->email;

        $admin = Admin::find($id);
        if (!$admin)
            return redirect()->route('admin.login')
                ->with('_ACCOUNT.RESET_FAIL', true);

        $datetime = Carbon::createFromFormat('YmdHi', $date);
        $dreset = Carbon::now()->subHours(1);

        $hash_token = base64_encode(hash_hmac('sha256', $date|$email, Config::get('app.reminder_key'), true));
        $hash_token = strtr($hash_token, ['/' => '_', '+' => '-', '=' => '~']);

        if ($datetime < $dreset || $token != $hash_token)
            return redirect()->route('admin.login')
                ->with('_ACCOUNT.RESET_FAIL', true);

        $admin->password = Hash::make($request->password);
        // $admin->resetpwd_at = Carbon::now()->addMinute(1);

        $admin->save();

        Mail::send('admin.email.reset_password', ['admin' => $admin], function($message) use ($admin)
        {
            $message->to($admin->email, $admin->name)->subject("Reset Password");
        });

        return redirect()->route('admin.login')
            ->with('_ACCOUNT.RESET_OK', true);
    }

    /**
     * Get the guard to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('admins');
    }
}
