<?php namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use File;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\Service;

class ServiceController extends Controller
{
    public function __construct(){
        $this->middleware('auth.admin');
    }

    public function index(Request $request, Builder $htmlBuilder){
        if ($request->ajax()) { 
            $services = Service::orderby('created_at','asc');

            return DataTables::of($services)
            ->addColumn('action', function($services){ return view('includes.backend._action', [
                'model' => $services,
                'edit_modal' => route('service.edit', $services->id),
                'delete' => route('service.delete', $services->id)
                ]);
            })    
            ->make(true);
        }

        $html = $htmlBuilder
        ->addColumn(['data'=>'name', 'name'=>'name', 'title'=>'Name'])
        ->addColumn(['data'=>'description', 'name'=>'description', 'title'=>'Description'])
        ->addColumn(['data' => 'action','name' => 'action', 'title' => 'Action', 'orderable' => false, 'searchable' => false, 'class' => 'text-center']);
        
        return view('admin.service.index',compact('html'));
    }

  

    public function store(Request $request){
        $service_path = Config::get('storage.service');
        if (!File::exists($service_path)) File::makeDirectory($service_path, 0777, true);

         $validator = Validator::make($request->all(), [
            'name' => 'required',
            'file_upload' => 'required',
        ]);

        if ($validator->passes()){ 
            $image = $request->file('file_upload');
            $random = Service::random($image);
            $_random = $random[1];
            $image_mimetype = $image->getMimeType(); 
            $image_resize = Image::make($image->getRealPath());
            $width = $image_resize->width();
            $height = $image_resize->height();

            $newWidth = 700;
            $diff = $width/$newWidth;
            $newHeight = $height/$diff;
            $image_resize->resize($newWidth, $newHeight);
            
            //if($width > 1920 && $height >1080)
            //  $image_resize->resize(1920, 1080);

            $service = Service::create([
                'slug' => str_slug($request->name),
                'name' => $request->name,
                'description' => $request->description,
                'filename' => $_random,
                'filename_mimetype' => $image_mimetype
            ]);

            if ($service->save() && $image)
                $image_resize->save($random[0].'/'.$random[1]);
            
            return response()->json(200);
        }else{
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ],400);
        }
    }

    public function show($id){
        //
    }

    public function edit($id){
        //
    }

    public function update(Request $request, $id){
        //
    }

    public function destroy($id){
        $service = Service::findOrFail($id);
        $image = $service->getFullPath();
        if ($service->delete() && File::exists($image))
            @unlink($image);
    }
}
