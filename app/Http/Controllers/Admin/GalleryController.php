<?php namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use File;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\Service;
use App\Models\Gallery;

class GalleryController extends Controller
{
   
    public function __construct(){
        $this->middleware('auth.admin');
    }

    public function index(Request $request, Builder $htmlBuilder){
        if ($request->ajax()) { 
            $galleries = Gallery::orderby('created_at','asc');

            return DataTables::of($galleries)
            ->editColumn('service_id',function ($galleries){
                return $galleries->service->name;
            })
            ->addColumn('action', function($galleries){ return view('includes.backend._action', [
                'model' => $galleries,
                'edit_modal' => route('gallery.edit', $galleries->id),
                'delete' => route('gallery.delete', $galleries->id)
                ]);
            })    
            ->make(true);
        }

        $html = $htmlBuilder
        ->addColumn(['data'=>'service_id', 'name'=>'service_id', 'title'=>'Service','searchable' => false])
        ->addColumn(['data'=>'name', 'name'=>'name', 'title'=>'name'])
        ->addColumn(['data' => 'action','name' => 'action', 'title' => 'Action', 'orderable' => false, 'searchable' => false, 'class' => 'text-center']);
        
        $services = Service::orderBy('name')
        ->pluck('name', 'id');

        return view('admin.gallery.index',compact('html','services'));
    }

    public function store(Request $request){
        $gallery_thumbnaile_storage = Config::get('storage.gallery_thumbnaile');
        $gallery_storage = Config::get('storage.gallery');

        if (!File::exists($gallery_thumbnaile_storage)) File::makeDirectory($gallery_thumbnaile_storage, 0777, true);
        if (!File::exists($gallery_storage)) File::makeDirectory($gallery_storage, 0777, true);

        $image = $request->file('file_upload');
        $random = Gallery::random($image);
        $_random = $random[1];
        $image_mimetype = $image->getMimeType(); 
        $image_resize = Image::make($image->getRealPath());
        $width = $image_resize->width();
        $height = $image_resize->height();

        $newWidth = 500;
        $diff = $width/$newWidth;
        $newHeight = $height/$diff;
        $image_resize->resize($newWidth, $newHeight);
        
        $gallery = Gallery::create([
            'service_id' => $request->service,
            'name' => $request->name,
            'description' => $request->description,
            'filename' => $_random,
            'filename_mimetype' => $image_mimetype
        ]);

        if ($gallery->save() && $image){
            $image->move($gallery_storage, $random[1]);
            $image_resize->save($gallery_thumbnaile_storage.'/'.$random[1]);
        }
           
        
        return response()->json(200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id){
        $gallery = Gallery::findOrFail($id);
        $image = $gallery->getFullPath();
        if ($gallery->delete() && File::exists($image))
            @unlink($image);
    }
}
