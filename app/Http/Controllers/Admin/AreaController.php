<?php namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session; 

//models
use App\Models\Area;

class AreaController extends Controller
{
    public function index(Request $request, Builder $htmlBuilder){
        if ($request->ajax()){ 
            $areas = Area::orderBy('created_at','asc')->where('is_show',true)
            ->orderby('order_by','asc');

            return DataTables::of($areas)
            ->addColumn('action', function($areas) {
                return view('_action', [
                    'model' => $areas,
                    'edit_modal' => route('area.edit',$areas->id),
                    'delete' => route('area.delete',$areas->id),
                ]);
            })     
            ->make(true);
        }

        $html = $htmlBuilder
        ->addColumn(['data'=>'name', 'name'=>'name', 'title'=>'NAME'])
        ->addColumn(['data'=>'description', 'name'=>'description', 'title'=>'DESCRIPTION'])
        ->addColumn(['data' => 'action','name' => 'action', 'title' => 'ACTION', 'orderable' => false, 'searchable' => false, 'class' => 'text-center', 'style'=>'width: 30px;']);
        
        return view('admin.area.index')
        ->with(compact('html'));
    }

    public function store(Request $request){
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
