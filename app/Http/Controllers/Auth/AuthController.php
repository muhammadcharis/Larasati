<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Customer;
use App\Models\SocialCustomer;

use Auth;
use Hash;
use Illuminate\Support\Facades\DB;
use Mail;
use Socialite;
use Session;

class AuthController extends Controller {

	public function __construct()
	{
		$this->middleware('guest', ['except' => [
			'getLogout'
		]]);
	}

	// public function loginRegister($page)
	// {
	// 	return view('simpleshop.account_login_register', compact('page'));
	// }

  public function login(){
   
  }

  public function register(){
    return view('register');
  }

	public function doLogin(Request $request)
	{
		if (Auth::customer()->check())
			return redirect()->back();

		$this->validate($request, [
			'email' => 'required', 'password' => 'required',
		]);

		//success login
		if (Auth::customer()->attempt([
			'email' => $request->email,
			'password' => $request->password,
			'state' => 'NEW'
			], true) || Auth::customer()->attempt([
			'email' => $request->email,
			'password' => $request->password,
			'state' => 'ACT'
			], true))
		{
			if($request->provider != "") {
				$customer = Auth::customer()->get();

				$social = $customer->socials()
					->where('provider', $request->provider)
					->first();

				if($social == null){
					$social = new SocialCustomer();
					$social->fill([
						'provider' => $request->provider,
						'provider_uid' => $request->provider_uid
					]);
				}else{
					$social->provider_uid = $request->provider_uid;
				}

				$customer->socials()->save($social);
			}

			if (Auth::customer()->get()->state == 'NEW')
				return redirect()->action('AccountController@showUnconfirmed');

			$next = session('next_redirect');
			if($next != null){
				session(['next_redirect' => null]);
				return redirect($next);
			}

			return redirect()->action('HomeController@index');
		}

		return redirect()->action('AuthController@loginRegister')
			->with('_ACCOUNT_INVALID', true);
	}

	public function doRegister(RegisterRequest $request)
	{
		$customer = new Customer();

		// $validator = \Validator::make($request->all(), [
		// 	'email' => 'unique:customers,email'
		// ]);

		// if ($validator->fails()) {
		// 	$customer = Customer::where('email', $request->email)->first();
		// 	if(!($customer != null && $customer->password == "*"))
		// 		return redirect('/auth/register')
		// 			->withErrors($validator)
		// 			->withInput()
		// 			->with('_ACCOUNT.REGISTER_FAIL', true);
		// }

		$customer->fill([
			'first_name' => $request->first_name,
			'last_name' => $request->last_name,
			'password' => Hash::make($request->password),
			'email' => $request->email,
			'phone' => '-',
			'state' => 'NEW',
		]);

		if ($customer->save()){
			if($request->provider != "") {
				$social = new SocialCustomer();
				$social->fill([
					'provider' => $request->provider,
					'provider_uid' => $request->provider_uid
				]);
				$customer->socials()->save($social);
			}
			Mail::send('email.account_confirmation', ['customer' => $customer], function($message) use ($customer)
			{
				$message->to($customer->email, $customer->name)->subject("Confirmation Account");
			});
			return redirect()->action('AuthController@registerFinish');
		} else {
			return redirect('/auth/register')->withInput();
		}

	}

	public function registerFinish()
	{
		return view('account_register_finish');
	}

	public function getLogout()
	{
		if (Auth::customer()->check())
			Auth::customer()->logout();

		return redirect()->action('AuthController@login');
	}

	public function redirect($provider)
	{
		return Socialite::driver($provider)->redirect();
	}

	public function callback($provider)
	{
		$user = Socialite::driver($provider)->user();

		$email = $user->getEmail();

		if($email == null){
			$social = SocialCustomer::where('provider', $provider)->where('provider_uid', $user->getId())->first();
			if($social == null) {
				$name = explode(" ", $user->getName());
				$firstName = implode(" ", array_slice($name, 0, 1));
				$lastName = implode(" ", array_slice($name, 1, sizeof($name)));
				return redirect()->action('AuthController@loginRegister', ['login'])
					->with('provider', $provider)
					->with('provider_uid', $user->getId())
					->with('first_name', $firstName)
					->with('last_name', $lastName);
			}else{
				$email = $social->customer->email;
			}
		}
		$result = $this->processSocialLogin($provider, $user->getId(), $user->getName(), $email);
		// var_dump($result);die;
		if($result == false){
			$name = explode(" ", $user->getName());
			$firstName = implode(" ", array_slice($name, 0, 1));
			$lastName = implode(" ", array_slice($name, 1, sizeof($name)));
			// return redirect()->action('AuthController@loginRegister', [$result])
			// 	->with('provider', $provider)
			// 	->with('provider_uid', $user->getId())
			// 	->with('email', $email)
			// 	->with('first_name', $firstName)
			// 	->with('last_name', $lastName);
			// var_dump($user->getName());die;
			$customer = new Customer();

			$customer->fill([
				'first_name' => $firstName,
				'last_name' => $lastName,
				'password' => Hash::make('12345678'),
				'email' => $email,
				'phone' => '62',
				'state' => 'ACT',
			]);
			if ($customer->save()){
				if($provider != "") {
					$social = new SocialCustomer();
					$social->fill([
						'provider' => $provider,
						'provider_uid' => $user->getId()
					]);
					$customer->socials()->save($social);
				}
			}
			\DB::commit();

			Auth::loginUsingId($customer->id, true);
		}

		// Auth::loginUsingId($customer->id, true);

		$next = session('next_redirect');
		if($next != null){
			session(['next_redirect' => null]);
			return redirect($next);
		}

		$races = $this->getRace();

		if(count($races) == 0)
			return redirect()->action('HomeController@index');
		else
			return redirect()->action('CartController@showCheckout');
	}

	private function processSocialLogin($provider, $id, $name, $email){
		\DB::beginTransaction();

		$customer = Customer::where('email', $email)->first();
		// var_dump($customer);die;
		if(count($customer) == 0){
			return false;
		}
		$social = $customer->socials()
			->where('provider', $provider)
			->where('provider_uid', $id)
			->first();

		if($social != null){
			Auth::loginUsingId($customer->id, true);
			return "login";
		}

		\DB::commit();
		Auth::loginUsingId($customer->id, true);

		return true;
	}

	static public function getRace()
	{
		return Session::get('race', array());
	}
}
