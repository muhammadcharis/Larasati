<?php

namespace App\Http\Controllers\Auth;

use DB;
use Mail;
use Auth;
use Hash;
use Session;
use Socialite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm(){
         return view('auth.login');
    }

    /*public function registerFinish(){
		return view('account_register_finish');
	}*/

    public function redirect($provider){
		return Socialite::driver($provider)->redirect();
	}

	public function callback($provider){
		$user = Socialite::driver($provider)->user();

		$email = $user->getEmail();

		if($email == null){
			$social = SocialCustomer::where('provider', $provider)->where('provider_uid', $user->getId())->first();
			if($social == null) {
				$name = explode(" ", $user->getName());
				$firstName = implode(" ", array_slice($name, 0, 1));
				$lastName = implode(" ", array_slice($name, 1, sizeof($name)));
				return redirect()->action('AuthController@loginRegister', ['login'])
					->with('provider', $provider)
					->with('provider_uid', $user->getId())
					->with('first_name', $firstName)
					->with('last_name', $lastName);
			}else{
				$email = $social->customer->email;
			}
		}
		$result = $this->processSocialLogin($provider, $user->getId(), $user->getName(), $email);
		// var_dump($result);die;
		if($result == false){
			$name = explode(" ", $user->getName());
			$firstName = implode(" ", array_slice($name, 0, 1));
			$lastName = implode(" ", array_slice($name, 1, sizeof($name)));
			// return redirect()->action('AuthController@loginRegister', [$result])
			// 	->with('provider', $provider)
			// 	->with('provider_uid', $user->getId())
			// 	->with('email', $email)
			// 	->with('first_name', $firstName)
			// 	->with('last_name', $lastName);
			// var_dump($user->getName());die;
			$customer = new Customer();

			$customer->fill([
				'first_name' => $firstName,
				'last_name' => $lastName,
				'password' => Hash::make('12345678'),
				'email' => $email,
				'phone' => '62',
				'state' => 'ACT',
			]);
			if ($customer->save()){
				if($provider != "") {
					$social = new SocialCustomer();
					$social->fill([
						'provider' => $provider,
						'provider_uid' => $user->getId()
					]);
					$customer->socials()->save($social);
				}
			}
			\DB::commit();

			Auth::loginUsingId($customer->id, true);
		}

		// Auth::loginUsingId($customer->id, true);

		$next = session('next_redirect');
		if($next != null){
			session(['next_redirect' => null]);
			return redirect($next);
		}

		$races = $this->getRace();

		if(count($races) == 0)
			return redirect()->action('HomeController@index');
		else
			return redirect()->action('CartController@showCheckout');
	}

	private function processSocialLogin($provider, $id, $name, $email){
		\DB::beginTransaction();

		$customer = Customer::where('email', $email)->first();
		// var_dump($customer);die;
		if(count($customer) == 0){
			return false;
		}
		$social = $customer->socials()
			->where('provider', $provider)
			->where('provider_uid', $id)
			->first();

		if($social != null){
			Auth::loginUsingId($customer->id, true);
			return "login";
		}

		\DB::commit();
		Auth::loginUsingId($customer->id, true);

		return true;
	}

    public function login(Request $request){
    	$this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);

    	$credentials = $this->credentials($request);
		if (Auth::validate($credentials)) {
            $user = Auth::getLastAttempted();
			//echo $this->redirectPath();
			//die();
			Auth::login($user, $request->has('remember'));
			return redirect()->intended($this->redirectPath());
            // $usr = $user->first();
           /* if ($user->state == 'ACT') {
                Auth::login($user, $request->has('remember'));
       		 	
            } else {
                return redirect()->action('AccountController@showUnconfirmed')
                    ->with('user', $user);
            }*/
        }

        return redirect('/auth/login')
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'email' => 'Incorrect email address or password',
            ]);
    }
}
