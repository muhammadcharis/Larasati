<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

use DB;
use Auth;
use Mail;
use Config;
use Validator;
use Carbon\Carbon;

use App\Models\User;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetForm(request $request){
        /*$date = $request->date;
        $token = $request->has_token;

        $user = User::find($id);
        if (!$user)
            return redirect()->action('AuthController@loginRegister')
                ->with('_ACCOUNT.RESET_FAIL', true);

        $datetime = Carbon::createFromFormat('YmdHi', $date);
        $dreset = Carbon::now()->subHours(1);

        $hash_token = base64_encode(hash_hmac('sha256', $date|$user->email, Config::get('app.reminder_key'), true));
        $hash_token = strtr($hash_token, ['/' => '_', '+' => '-', '=' => '~']);

        if ($datetime < $dreset || $token != $hash_token)
            return redirect()->action('AuthController@loginRegister')
                ->with('_ACCOUNT.RESET_FAIL', true);

        return view('auth.passwords.reset')->with(
            ['token' => $token, 'user' => $user, 'date' => $date]
        );*/
        return view('auth.passwords.reset');
    }

    public function reset(request $request){
        $validator =  Validator::make($request->all(), [ 
            'nik' => 'required', 
            'password_new' => 'required', 
            'password_confirm' => 'required|same:password_new', 
        ]); 
        if ($validator->passes()) { 
            $nik = $request->nik;
            $password = $request->password_new;

            $user = User::where('nik',$nik)->first();
            if (!$user)
                return response()->json('USER TIDAK DITEMUKAN',422);

           // $datetime = Carbon::createFromFormat('YmdHi', $date);
            //$dreset = Carbon::now()->subHours(1);

            //$hash_token = base64_encode(hash_hmac('sha256', $date|$email, Config::get('app.reminder_key'), true));
            //$hash_token = strtr($hash_token, ['/' => '_', '+' => '-', '=' => '~']);

            /*if ($datetime < $dreset || $token != $hash_token)
                return redirect()->route('admin.login')
                    ->with('_ACCOUNT.RESET_FAIL', true);*/

             try{ 
                db::beginTransaction(); 
                $user->password = bcrypt($password); 
 
                if($user->update()){ 
                    db::commit(); 
                    dispatch(new SendResetPasswordEmail($user));   
                    /*Mail::to('email.reset_password', function($message) use ($user)
                    {
                        $message->from('no-reply@aoi.co.id','WMS System');
                        $message->to($user->email);
                        $message->subject("Reset Password");
                    })
                    ->queue(new SendResetPasswordEmail($user));*/

                    return response()->json('success', 200); 
                } 
                 
            }catch (Exception $ex){ 
                db::rollback(); 
                $message = $ex->getMessage(); 
                ErrorHandler::db($message);  
            } 
        }else{
            return response()->json([ 
                'errors' => $validator->getMessageBag()->toArray() 
            ],400);  
        }
        
    }
}
