<?php

use \App\Vendor\Lokavor\I18lFilter;

use App\Models\Customer;
use Carbon\Carbon;

if (!function_exists('cart'))
{
	function cart() {
		return App\Http\Controllers\CartController::getCart();
	}
}

if (!function_exists('getProvince'))
{
	function getProvince($id = null) {

		$key = Config::get('app.rajaongkir');
		$url = "http://pro.rajaongkir.com/api/province";
		if($id != null){
			$url .= '?id='.$id;
		}

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "key:".$key
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
			// var_dump($response);die;
		  	return $response;
		}
	}
}

if (!function_exists('getCity'))
{
	function getCity($province_id = null, $city_id = null) {
		$key = Config::get('app.rajaongkir');
		$url = "http://pro.rajaongkir.com/api/city";
		if($province_id != null && $city_id == null){
			$url .= '?province='.$province_id;
		}elseif ($province_id != null && $city_id != null) {
			$url .= '?id='.$city_id.'&province='.$province_id;
		}

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "key:".$key
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return $response;
		}
	}
}

if(!function_exists('getSubdistrict')){

	function getSubdistrict($city_id = null, $subdistrict_id = null){

		$key = Config::get('app.rajaongkir');
		$url = "http://pro.rajaongkir.com/api/subdistrict";
		if($city_id != null && $subdistrict_id == null){
			$url .= '?city='.$city_id;
		}elseif ($city_id != null && $subdistrict_id != null) {
			$url .= '?id='.$subdistrict_id.'&city='.$city_id;
		}
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  	CURLOPT_URL => $url,
		  	CURLOPT_RETURNTRANSFER => true,
		  	CURLOPT_ENCODING => "",
		  	CURLOPT_MAXREDIRS => 10,
		  	CURLOPT_TIMEOUT => 30,
		  	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  	CURLOPT_CUSTOMREQUEST => "GET",
		  	CURLOPT_HTTPHEADER => array(
		    	"key:".$key
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return $response;
		}
	}
}

if(!function_exists('getCost')){

	function getCost($origin = null, $origin_type = null, $destination = null, $destination_type = null, $weight = null, $courier = null){

		$key = Config::get('app.rajaongkir');
		// origin=501&originType=city&destination=574&destinationType=subdistrict&weight=1700&courier=jne
		$postFields = 'origin='.$origin.'&originType='.$origin_type.'&destination='.$destination.'&destinationType='.$destination_type.'&weight='.$weight.'&courier='.$courier;

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://pro.rajaongkir.com/api/cost",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $postFields,
		  CURLOPT_HTTPHEADER => array(
		    "content-type: application/x-www-form-urlencoded",
		    "key:".$key
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return $response;
		}
	}
}

if(!function_exists('generateCode'))
{
	function generateCode(){
		$char = 'ABCDEFGHIJKLMOPQRSTUVXWYZ0123456789'; 
		$base = strlen($char); 
		$base--; 

		$code=NULL; 
	    
	    do{
	    	for($x=1;$x<=5;$x++){ 
		        $i = rand(0,$base); 
		        $code .= substr($char,$i,1); 
		    } 
	    }while (Customer::where('referal_code', '=', $code)->first() != null);
           
		return $code;
	}
}

if (!function_exists('i18l_date'))
{
	function i18l_date($value, $pattern = array(), $locale = null, $timezone = null) {
		return I18lFilter::date($value, $pattern, $locale, $timezone);
	}
}

if (!function_exists('i18l_currency'))
{
	function i18l_currency($value, $locale = null) {
		$a = str_replace("IDR", "IDR ", I18lFilter::currency($value, $locale));
		$a = str_replace("SGD", "SGD ", $a);
		return $a;
	}
}

if (!function_exists('i18l_number'))
{
	function i18l_number($value, $pattern = null, $locale = null) {
		return I18lFilter::number($value, $pattern, $locale);
	}
}