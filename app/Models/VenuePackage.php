<?php namespace App\Models;

use Config;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;


class VenuePackage extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = [
        'venue_id','description'
    ];

    public function venue()
    {
        return $this->belongsTo('App\Models\Venue');
    }
}
