<?php namespace App\Models;

use DB;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $fillable = [
        'name', 'email', 'password','is_active'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['created_at','deleted_at'];

    public function Areas(){
        return $this->hasMany('App\Models\Area');
    }
}   
