<?php namespace App\Models;

use Config;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Venue extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = [
        'name'
    ];

    public function venuePackage(){
        return $this->hasMany('App\Models\VenuePackage');
    } 
}
