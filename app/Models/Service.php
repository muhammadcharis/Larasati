<?php namespace App\Models;

use Config;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = [
        'name','description', 'filename', 'filename_mimetype','slug'
    ];
    protected $dates = ['created_at'];

    public function galleries(){
        return $this->hasMany('App\Models\Gallery'); 
    }

    public function getFullPath(){
        return Config::get('storage.service') . '/' . e($this->filename);
    }

 	static function random($image){
        $ret = [];
        $path = Config::get('storage.service');
        $extension = $image->getClientOriginalExtension();
        $try = 250;

        do {
            if ($try <= 0)
                throw Exception("Failed to produce randomized filename");

            $hash = static::cleanFilename(str_random(32));
            if ($extension)
                $hash = $hash . '.' . $extension;
            $file = $path . '/' . $hash;
            $try -= 1;
        } while (file_exists($file));

        $ret = [$path, $hash];
        return $ret;
    }

    static function cleanFilename($value){
        $pattern = '/[^a-zA-Z0-9\-_\.& \(\)]+/';
        $replacement = '-';
        $value = preg_replace($pattern, $replacement, $value);
        return $value;
    }

   

}
