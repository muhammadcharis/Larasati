<?php namespace App\Models;

use Config;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Timeline extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = [
        'description', 'filename', 'filename_mimetype','show_order','timeline_date'
    ];
    protected $dates = ['created_at','timeline_date'];

    public function getFullPath(){
        return Config::get('storage.timeline') . '/' . e($this->filename);
    }

 	static function random($image){
        $ret = [];
        $path = Config::get('storage.timeline');
        $extension = $image->getClientOriginalExtension();
        $try = 250;

        do {
            if ($try <= 0)
                throw Exception("Failed to produce randomized filename");

            $hash = static::cleanFilename(str_random(32));
            if ($extension)
                $hash = $hash . '.' . $extension;
            $file = $path . '/' . $hash;
            $try -= 1;
        } while (file_exists($file));

        $ret = [$path, $hash];
        return $ret;
    }

    static function cleanFilename($value){
        $pattern = '/[^a-zA-Z0-9\-_\.& \(\)]+/';
        $replacement = '-';
        $value = preg_replace($pattern, $replacement, $value);
        return $value;
    }
}
