<?php namespace App\Models;

use DB;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    use Uuids;

    public $incrementing = false;
    protected $guarded = ['id'];
    protected $dates = ['created_at','deleted_at'];
    protected $fillable = ['order_category_id','detail_description','qty','price','uom'];

    public function orderCategory(){
        return $this->belongsTo('App\Models\OrderCategory');
    }
}
