<?php namespace App\Models;

use DB;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class OrderCategory extends Model
{
    use Uuids;

    public $incrementing = false;
    protected $guarded = ['id'];
    protected $dates = ['created_at','deleted_at'];
    protected $fillable = ['order_id','name','total_per_category','uom','qty'];
    
    public function order(){
        return $this->belongsTo('App\Models\Order');
    }

     public function orderDetail(){
        return $this->hasMany('App\Models\OrderDetail');
    } 
}
