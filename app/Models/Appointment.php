<?php namespace App\Models;

use Config;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $dates = ['date_appointment'];
    protected $fillable = [
        'name','email', 'no_tlpn', 'date_appointment','appointment_type','description'
    ];
    
}
