<?php namespace App\Models;

use DB;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Area extends Model
{
    use Uuids;
    use SoftDeletes;
    
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = ['name','description','created_at','admin_id','order_by','is_show'];
    protected $dates = ['created_at','deleted_at'];
    
    public function admin(){
        return $this->belongsTo('App\Models\Admin');
    }
}
