<?php namespace App\Models;

use DB;
use App\Uuids;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use Uuids;

    public $incrementing = false;
    protected $guarded = ['id'];
    protected $dates = ['created_at','deleted_at','order_date_from','order_date_to'];
    protected $fillable = ['name','email','venue','phone_number','event_name','event_location','order_date_from','order_date_to','order_number','total_order','discount','total_order_after_discount'];
    

    public function orderCategory(){
        return $this->hasMany('App\Models\OrderCategory');
    } 

    static function createOrderNumber(){
        $count = 1;
        $find = Order::whereRaw("to_char(created_at,'YYYY') = '".Carbon::now()->format('Y')."'")->count();
        
        if ($find > 0){
            $count = $find+1;   
            if ($count < 10) return 'OL/00'.$count.'/'.Carbon::now()->format('ymd');
            else if ($count < 100) return 'OL/0'.$count.'/'.Carbon::now()->format('ymd');
            else return 'OL/'.$count.'/'.Carbon::now()->format('ymd');
        }
           

		return 'OL/00'.$count.'/'.Carbon::now()->format('ymd');
    }
}
