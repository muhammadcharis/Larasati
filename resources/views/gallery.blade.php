@extends('layouts.frontend',
    [
        'title'=>'Galeri',
        'active'=>'gallery',
        'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
    ]
)

@section('content-css')
    <link href="{{ mix('css/lightbox.min.css') }}" rel="stylesheet">

    <style type="text/css">
    </style>
@endsection

@section('content')
     @include('includes._page-header',['ptitle' => 'Galeri','bgimg'=>'/images/bouquet.jpg'])  

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 main-title">
                    <h3 class="text-center">Punya Pertanyaan? Jangan ragu untuk bertanya atau datang ke kantor kami.<br>Kami Senang bisa menghubungi Anda</h3>
                </div>
            </div>
        </div>
    </section>
    
    <section id="gallery">
        <div class="container">
            <div class="row">
                <div role="tabpanel" class="lar-tab">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs text-center" role="tablist">
                        @foreach ($galleries as $key => $service)
                            <li role="presentation" class="text-center text-uppercase {{ (isset($key)&&($key==0)) ? 'active' : '' }}">
                                <a href="#{{$service->slug}}" aria-controls="{{$key+1}}" role="tab" data-toggle="tab"><h1> {{ ucwords($service->service_name) }}</h1></a>
                            </li>
                        @endforeach
                    </ul>
                
                    <div class="tab-content">
                        @foreach ($galleries as $key => $gallery)
                            <div role="tabpanel" class="tab-pane {{ (isset($key)&&($key==0)) ? 'active' : '' }}" id="{{$gallery->slug}}">
                                <div class="col-xs-12 text-center">
                                    <p>{{ $gallery->description}}.</p>
                                </div>
                                <div class="col-xs-12">
                                    <div class="gall">
                                        @foreach ($gallery->details as $key => $detail)
                                            <div class="gall-coll">
                                                <a href="{{ $detail->gallery_image }}" data-lightbox="all" data-title="{{ $detail->title }}">
                                                    <div class="gall-item">
                                                        <div class="gal-img">
                                                            <img src="{{ $detail->gallery_image_tumbnail }}">
                                                        </div>
                                                        <div class="gal-overlay">
                                                            <div class="gal-ov-desc">
                                                                <i class="fas fa-search fa-3x"></i><br>
                                                                <p>{{ $detail->description }}</p>
                                                            </div>
                                                            <div class="gal-ov-tag">
                                                                 <p>{{ $detail->service_name }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
  
                </div>
            </div>
            
        </div>
    </section>

    @include('includes._askus')  

@endsection

@section('modal') 
@endsection

@section('content-js')
<script src="{{ mix('js/masonry.pkgd.min.js') }}"></script>
<script src="{{ mix('js/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ mix('js/lightbox.min.js') }}"></script>


<script type="text/javascript">
    $(function () {
        // body...
        var $container = $('.gall');
        $container.imagesLoaded( function () {
          $container.masonry({
              itemSelector: '.gall-coll', // use a separate class for itemSelector, other than .col-
              columnWidth: '.gall-coll',
            });  
        });

        //Reinitialize masonry inside each panel after the relative tab link is clicked - 
        $('a[data-toggle=tab]').each(function () {
            var $this = $(this);

            $this.on('shown.bs.tab', function () {
            
                $container.imagesLoaded( function () {
                    $container.masonry({
                        columnWidth: '.gall-coll',
                        itemSelector: '.gall-coll',
                    });
                });

            }); //end shown
        });  //end each
    });
    $(function(){
        var hash = window.location.hash;
        hash && $('ul.nav a[href="' + hash + '"]').tab('show');
    });
</script> 
@endsection


