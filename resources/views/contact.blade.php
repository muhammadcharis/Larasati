@extends('layouts.frontend',
            [
                'title'=>'Kontak',
                'active'=>'contact',
                'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
            ]
        )

@section('content-css')
<style type="text/css">
</style>
@endsection

@section('content')
     @include('includes._page-header',['ptitle' => 'Kontak','bgimg'=>'/images/bouquet.jpg'])  

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 main-title">
                    <h3 class="text-center">Punya Pertanyaan? Jangan ragu untuk bertanya atau datang ke kantor kami.<br>Kami Senang bisa menghubungi Anda</h3>
                </div>
            </div>
        </div>
    </section>
     <section class=" bluelight">
        <div class="container">
            <div class="row contact-body">
                    <p><strong>ALAMAT</strong><br>
                        <i class="fas fa-map-marker-alt"></i><span> Jl. T M Pahlawan No. 4, Kabupaten Blora, Jawa Tengah.</span></p>
                    <p><strong>NO. TLPN</strong><br>
                        <i class="fas fa-phone"></i><span> (+62296) 532074</span></p>
                    <p><strong>EMAIL</strong><br>
                        <i class="far fa-envelope"></i> <span>larasatiblora@gmail.com</span></p>
                    <p><strong>SOSIAL MEDIA</strong></p>
                    <p class="soc-icon">
                        <p><span><a style="color:black;" href="https://www.instagram.com/larasatidekor_blora/" target="_blank" title="instagram"><i class="fab fa-instagram"></i> Larasati Blora Decoration </a></span></p>
                        {{--<span><a href="https://api.whatsapp.com/send?phone=6281294633942&text=Saya%20tertarik%20dengan%20dekorasi%20pernikahan%20anda" title="whatsapp" target="_new"><i class="fab fa-whatsapp "></i></a></span>--}}
                    </p>
            </div>
        </div>
    </section>
    <section>
        <div class="container-fluid">
            <div class="row">
                <iframe
                  width="100%"
                  height="500px"
                  frameborder="0" style="border:0"
                  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.3923828267248!2d111.42838031477953!3d-6.962953670109084!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7740565f7fe177%3A0xba34f472af2c84f6!2sLarasati+Griya+Rias+Dan+Dekorasi!5e0!3m2!1sen!2sid!4v1538397145171" allowfullscreen>
                </iframe>
            </div>
        </div>
    </section>

    @include('includes._askus')  

@endsection

@section('modal') 
@endsection

@section('content-js') 

@endsection


