@extends('layouts.frontend',
            [
                'title'=>'Home',
                'active'=>'home',
                'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
            ]
        )

@section('content-css')
@endsection

@section('content')
    <section>
        <div id="homeCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#homeCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#homeCarousel" data-slide-to="1"></li>
                <li data-target="#homeCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <img src="http://lorempixel.com/1400/1200/sports/1/Dummy-Text/" alt="Los Angeles">
                </div>

                <div class="item">
                    <img src="http://lorempixel.com/1400/800/cats/1/Dummy-Text/" alt="Chicago">
                </div>

                <div class="item">
                    <img src="http://lorempixel.com/1200/1200/food/1/Dummy-Text/" alt="New York">
                </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#homeCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#homeCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>
@endsection

@section('modal') 
@endsection

@section('content-js') 
@endsection


