<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="{{ route('logo')}}">
        <meta name="description" content="Larasati Blora adalah suatu badan yang bergerak di bidang pernikahan meliputi dekorasi, rias, dan persewaan gedung">
        <meta name="author" content="Larasati Blora">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="robots" content="index, follow">
         <meta name="keywords" content="Larasati Blora,Wedding Service,Jasa Dekorasi,Dekorasi,Blora,Aspedi,Jawa Tengah,Dekorasi Blora,Dekorasi Pati,Dekorasi Cepu,Rias,Rias Pengantin,Gedung Nikah,Gedung Blora,Sewa Gedung">

        <meta property="og:type" content="Company Profile" />
        <meta property="og:title" content="Larasati Blora"/>

        <meta property="og:description" content="Larasati Blora adalah suatu badan yang bergerak di bidang pernikahan meliputi dekorasi, rias, dan persewaan gedung" />

        <meta property="og:image" content="{{ route('logo')}}"/>

        <meta property="og:url" content={{ url()->current() }} />
        <meta property="og:site_name" content="{{ url()->current() }}" />

        <title>Larasati | {{{ isset($title) ? $title : 'Larasti' }}}</title>

        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:400,700|Raleway:300,400,500,700,900" rel="stylesheet">
        <link href="{{ mix('css/costum.css') }}" rel="stylesheet">
        <link href="{{ mix('css/datepicker.min.css') }}" rel="stylesheet">
        <link href="{{ mix('css/animate.css') }}" rel="stylesheet">
       @yield("content-css")
    </head>

    <body>
        <div id="app">
        <header>
            @include('includes._navigation')  
        </header>

        <div class="main-content-wrapper">
            @yield('content')
        </div>

        <footer>
            @include('includes._footer')  
        </footer>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <modal-appoitment-component> </modal-appoitment-component>
        
        @yield('modal')
        </div>
        <script src="{{ mix('js/app.js') }}"></script>
        <script src="{{ mix('js/costum.js') }}"></script>
        <script type="text/javascript" src="{{ mix('js/datepicker.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/mouse0270-bootstrap-notify/3.1.7/bootstrap-notify.js"></script>
        {{-- <script src="{{ asset('js/custom.js') }}"></script> --}}

        @yield('content-js')
        <script type="text/javascript">
            $(document).ready(function(){
                var mainNavbar = $("#mainNavbar");
                var cpHomeS = $(".cpHomeS");
                var carIner = $(".carousel-inner");
                var pageHeader = $(".pg-header-space");

                //venue
                var venSlider = $('.venue-slider');
                var venAbout = $('.about-venue');
                var venList = $('.list-venue');

                venList.css('height',venSlider.height()-60);
                venAbout.css('height',venSlider.height()-60);
                //endvenue
                //asksection
                var askDesc = $('.ask-text');
                var askBtn = $('.ask-btn');

                askBtn.css('height',askDesc.height()+60);
                //endasksection

                cpHomeS.css('margin-top',mainNavbar.height());
                cpHomeS.css('height',(carIner.height() - mainNavbar.height()));
                pageHeader.css('margin-top',mainNavbar.height());

                $(window).resize(function(){

                    cpHomeS.css('margin-top',mainNavbar.height());
                    cpHomeS.css('height',(carIner.height() - mainNavbar.height()));
                    pageHeader.css('margin-top',mainNavbar.height());

                    venList.css('height',venSlider.height()-60);
                    venAbout.css('height',venSlider.height()-60);

                    //asksection
                    askBtn.css('height',askDesc.height()+60);
                    //endasksection
                    
                }); 

            });

            $('#formAppointment').submit(function(event){
                event.preventDefault();
                console.log('asd');
            });
        </script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        {{-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> --}}
    </body>
</html>
