<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Larasati | Admin</title>

	<!-- Global stylesheets -->
	<link href="{{ mix('css/backend.css') }}" rel="stylesheet" type="text/css">
	
	<link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- /global stylesheets -->
</head>

<body>
	@include('includes.backend.main_navbar')
	@include('includes.backend.second_navbar',['active' => $menu_active])
	@include('includes.backend.page_header')
	@include('includes.backend.top_notif')
	
	<div class="page-container">

		<div class="page-content">
			<div class="content-wrapper">
				@yield('content')
				@yield('modal')
			</div>
		</div>
	</div>
	
	<!-- /footer -->
	<script src="{{ mix('js/backend.js') }}"></script>
	<script src="{{ mix('js/notifications.js') }}"></script>
	<script src="{{ mix('js/bootbox.js') }}"></script>
	<script src="{{ mix('/js/mustache.js') }}"></script>
	<script src="{{ mix('js/pickadate.js') }}"></script>
	<script src="{{ mix('js/datepicker.js') }}"></script>
	
	<script type="text/javascript">
		Mustache.tags = ['{%', '%}'];
		$(function(){
			var dtable = $('#dataTableBuilder').dataTable().api();

			$(".dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtable.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtable.search("").draw();
					}
					return;
			}); 
		})
	</script>
	@yield('content-js')
</body>
</html>
