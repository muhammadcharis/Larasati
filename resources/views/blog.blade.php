@extends('layouts.frontend',
            [
                'title'=>'Blog',
                'active'=>'blog',
                'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
            ]
        )

@section('content-css')
<style type="text/css">
</style>
@endsection

@section('content')
     @include('includes._page-header',['ptitle' => 'Blog','bgimg'=>'/images/bouquet.jpg'])  

    <section id="homeBlog">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 main-title">
                    <h1 class="text-center">Get Inspired</h1>
                    <p class="col-md-6 col-md-offset-3 text-center">Browse our blog for tips and inspiration. From real wedding to the latest trends. Let us inspire you.</p>
                </div>
            </div>
            <div class="row blogs-gal">
                @for($key=0;$key<9;$key++)
                @if($key%3 == 0)
                    <div class="col-xs-12 no-p">
                @endif
                <a href="{{route('blog.detail','sluuuug')}}">
                    <div class="col-sm-4 col-xs-12">
                        <div class="blog-item">
                            <div class="img-box">
                                <img src="{{asset('images/bonquet.jpg')}}">
                            </div>
                            <div class="blog-desc">
                                <h3>Beach Outdoor Party on White Sands {{$key}}</h3>
                                <p class="text-red">15 Feb 2018</p>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. <span class="text-red"> {...} </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </a>
                @if($key%3==2)
                    </div>
                @endif
                @endfor
            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <a href="#" class="btn btn-red-o text-uppercase mt-25">Other Blog</a>
                </div>
            </div>
        </div>
    </section>

    @include('includes._askus',['askcolor' => 'peach'])  

@endsection

@section('modal') 
@endsection

@section('content-js') 
@endsection


