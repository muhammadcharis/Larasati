@extends('layouts.frontend',
            [
                'title'=>'Gedung',
                'active'=>'venue',
                'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
            ]
        )

@section('content-css')
    <link href="{{ mix('css/fullcalendar.css') }}" rel="stylesheet">
    <link href="{{ mix('css/fullcalendar.print.css') }}" rel="stylesheet" media='print'>

<style type="text/css">
</style>
@endsection

@section('content')

    <slider-venue-component></slider-venue-component>
    <section class="venue">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 main-title pt-50">
                    <h1 class="text-center text-red">Graha Larasati</h1>
                </div>
            </div>
            <div class="row pb-50">
                <div role="tabpanel" class="lar-tab">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active col-md-offset-2 col-md-4 col-sm-6 col-xs-12 text-center text-uppercase">
                            <a href="#description" aria-controls="description" role="tab" data-toggle="tab"><h1>Deskripsi</h1></a>
                        </li>
                        <li role="presentation" class="col-md-4 col-sm-6 col-xs-12 text-center text-uppercase">
                            <a href="#package" aria-controls="package" role="tab" data-toggle="tab"><h1>Paket</h1></a>
                        </li>
                    </ul>
                
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="description">
                            <div class="col-xs-12">
                                <p>
                                    <strong class="text-uppercase">Graha Larasati</strong> adalah Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </p>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </p>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </p>
                                <p><strong>Alamat</strong></p>
                                <p>Jl. T M Pahlawan No. 4, Kabupaten Blora, Jawa Tengah.</p>
                            </div>
                        </div>
                        <package-venue-component></package-venue-component>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
    <section id="askus" class="">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="ask-text">
                        <h3>
                            <strong>Lihat jadwal kami di bawah ini, dan pesan untuk hari anda</strong>
                        </h3>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="ask-btn">
                        <div class="ask-btn-v">
                            <btn-appoitment-component @button_clicked="lunchModal()"></btn-appoitment-component>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="ven-calendar">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div id='calendar'></div>
                </div>
            </div>
        </div>
	</section>
    

@endsection

@section('modal') 
@endsection

@section('content-js') 
<script src="{{ mix('js/fullcalendar.js') }}"></script>

<script type="text/javascript">

    $(document).ready(function() {
        setInterval(function () {
            $('#calendar').fullCalendar('refetchEvents');
            $('#calendar').fullCalendar('rerenderEvents');
        }, 10000);

        $('#calendar').fullCalendar({
            themeSystem: 'bootstrap3',
            header: {
                left: 'title',
                center: 'month,listMonth',
                right: 'prev,next today'
            },
            defaultDate: getDateNow(),
            weekNumbers: true,
            navLinks: true, // can click day/week names to navigate views
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: '/get-venue-agenda',
            timeFormat: 'h:mmt' 
        });
    });

    function getDateNow(){
        var date = new Date();
        var _year = date.getFullYear();
        var _month = parseInt(date.getMonth())+1;
        if(_month < 10) _month = '0'+_month;
        else _month;

        var _date = date.getDate();
        if(_date < 10) _date = '0'+_date;
        else _date;

        return _year+'-'+_month+'-'+_date;
    }

</script>
@endsection


