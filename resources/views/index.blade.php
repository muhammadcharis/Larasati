@extends('layouts.frontend',
            [
                'title'=>'Beranda',
                'active'=>'home',
                'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
            ]
        )

@section('content-css')
    <link href="{{ mix('css/owl-carousel.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    
    <slider-component></slider-component>

    <section class="sec-def ptb-50" id="whyus">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 main-title">
                    <h1 class="text-center">Mengapa Memilih Kami ?</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 col-xs-6 why-item">
                    <div class="img-box text-center">
                        <img src="{{asset('images/time.png')}}">
                    </div>
                    <div class="desc text-center">
                        <p>
                            <strong>Since 1992</strong>
                            <br>
                            Memiliki pengalaman lebih dari 25 tahun dan jam terbang yang cukup tinggi di bidang pernikahan. 
                        </p>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6 why-item">
                    <div class="img-box text-center">
                        <img src="{{asset('images/price.png')}}">
                    </div>
                    <div class="desc text-center">
                        <p>
                            <strong>Customize</strong>
                            <br>
                            Kami melayani paket all-in-one yang juga menyediakan jasa wedding organizer, gedung pernikahan, dekorasi, rias pengantin, fotografi, dll dan paket lainnya sesuai dengan kebutuhan. Berbagai macam konsep juga dapat kami wujudkan sesuai dengan pernikahan impian calon pengantin.
                        </p>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6 why-item">
                    <div class="img-box text-center">
                        <img src="{{asset('images/save.png')}}">
                    </div>
                    <div class="desc text-center">
                        <p>
                            <strong>Accessible</strong>
                            <br>
                            Calon pengantin dapat melakukan komunikasi dengan tim kami via WhatsApp atau melakukan pemesanan gedung secara online.
                        </p>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6 why-item">
                    <div class="img-box text-center">
                        <img src="{{asset('images/number.png')}}">
                    </div>
                    <div class="desc text-center">
                        <p>
                            <strong>Up-to-Date</strong>
                            <br>
                            Inovasi adalah salah satu prinsip yang selalu kami pegang karena perkembangan dunia pernikahan cepat berubah.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="featured1"  class="parallax" style="background-image: url('/images/bonquet.jpg');">
        <div class="parralax-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h1>Larasati One Stop Wedding Service</h1>
                </div>
            </div>
        </div>
    </section>

    <service-component></service-component>

    <br/>

    {{--<section id="testimoni"  class="parallax" style="background-image: url('/images/bonquet.jpg');">
        <div class="parralax-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 main-title">
                    <h1 class="text-center">Our Happy Clients</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div id="carouselTesti" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                        @for($key=0;$key<3;$key++)
                            <div class="item {{ (isset($key)&&($key==0)) ? 'active' : '' }}">
                                <div class="row testi-item">
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="img-box">
                                            <img src="{{asset('images/bride.jpg')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                        <h3>Budi dan Ani {{$key}}</h3>
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endfor
                        </div>
                        <a class="left carousel-control" href="#carouselTesti" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                        <a class="right carousel-control" href="#carouselTesti" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>--}}

    <section id="partners" class="ptb-50">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 main-title">
                    <h1 class="text-center">Insta Of The Week</h1>
                    <p ><h2 class="text-center"><a href="https://www.instagram.com/larasatidekor_blora/" style="color:#E43137"target="_blank" title="instagram"><i class="fab fa-instagram"></i></a></h2></p>
                </div>
            </div>
            <instagram-feed-component></instagram-feed-component>
        </div>
    </section>
    
@endsection

@section('modal') 
@endsection

@section('content-js') 
<script src="{{ mix('js/owl-carousel.min.js') }}"></script>

@endsection


