@extends('layouts.app_admin')

@section('content')
<div class="wrapper error-page pa-0">
<header class="sp-header">
    <div class="sp-logo-wrap pull-left">
    </div>
    <div class="clearfix"></div>
</header>

<!-- Main Content -->
<div class="page-wrapper pa-0 ma-0 error-bg-img">
    <div class="container-fluid">
        <!-- Row -->
        <div class="table-struct full-width full-height">
            <div class="table-cell vertical-align-middle auth-form-wrap">
                <div class="auth-form  ml-auto mr-auto no-float">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="mb-30">
                                 <img class="brand-img mb-10" style="margin-left:25%;width: 50%;" src="{{ asset('img/logo.png') }}" alt="brand"/>
                                 <p class="text-center">Stay tune we're launching soon</p>
                            </div>  
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->   
    </div>
    
</div>
<!-- /Main Content -->

</div>
@endsection