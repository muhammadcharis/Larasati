<div id="insertModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			
				<form action="{{ route('slider.store') }}" type="post" class="form-horizontal" id="insert_slider" enctype="multipart/form-data">
				 {{ csrf_field() }}
				<div class="modal-body">
					@include('form.textarea', [
						'field' => 'description',
						'label' => 'Description',
						'label_col' => 'col-xs-2',
						'form_col' => 'col-xs-10',
						'attributes' => [
							'id' => 'description'
						]
					])

					@include('form.file', [
						'field' => 'file_upload',
						'label' => 'Image',
						'label_col' => 'col-xs-2',
						'form_col' => 'col-xs-10',
						'attributes' => [
							'id' => 'file_upload'
						]
					])

					@include('form.select', [
						'label' => 'Show On',
						'field' => 'show_on',
						'options' => [
							'booth' => 'Booth',
							'home' => 'Home',
							'venue' => 'Venue'
						],
						'label_col' => 'col-xs-2',
						'form_col' => 'col-xs-10',
						'attributes' => [
							'id' => 'show_on'
						]
					])

					

				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Submit form</button>
				</div>
			</form>
		</div>
	</div>
</div>