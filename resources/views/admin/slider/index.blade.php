@extends('layouts.backend', ['menu_active' => 'slider'])

@section('title')
	<h4><i class="icon-exclamation position-left"></i> <span class="text-semibold">Company Profile </span> - Slider</h4>
	<ul class="breadcrumb breadcrumb-caret position-right">
		<li><a href="{{ route('admin.home')}}">Company Profile</a></li>
		<li class="active">Slider</li>
	</ul>
@endsection

@section('content')
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<a href="#" class="btn btn-default" data-toggle="modal" data-target="#insertModal"><i class="icon-plus2 position-left"></i>Create New</a>
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table datatable-show-all table-striped']) !!}
			</div>
		</div>
	</div>
@endsection

@section('modal')
	@include('admin.slider._insert_modal')
@endsection

@section('content-js')
{!! $html->scripts() !!}
<script>
	$('#is_show_on_home').on('change',function(){
		$(this).val(this.checked ? true : false);
	});

	$('#is_show_on_venue').on('click',function(){
		$(this).val(this.checked ? true : false);
	});

	$('#insert_slider').submit(function (event) {
		event.preventDefault();
		bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
			if(result){
				$.ajax({
					type: "POST",
					url: $('#insert_slider').attr('action'),
					data:new FormData($("#insert_slider")[0]),
					processData: false,
					contentType: false,
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
						$('#insertModal').modal('hide');
					},
					complete: function () {
						$.unblockUI();
						$('#insertModal').modal('hide');
					},
					success: function (response) {
						$('#insert_slider').trigger("reset");
						$('#dataTableBuilder').DataTable().ajax.reload();
						$("#alert_success").trigger("click", 'Data Berhasil disimpan');
					},
					error: function (response) {
						$.unblockUI();
						$('#insertModal').modal();

						if (response.status == 400) {
							$("#alert_error").trigger("click", 'file foto wajib di isi');
						} 
					}
				});
			}
		});
	});

	function hapus(url){
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		
		$.ajax({
			type: "delete",
			url: url,
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			success: function (response) {
				$.unblockUI();
			},
			error: function (response) {
				$.unblockUI();
			}
		}).done(function ($result) {
			$('#dataTableBuilder').DataTable().ajax.reload();
			$("#alert_success").trigger("click", 'Data Berhasil hapus');
		});
	}
</script>
@endsection
