@extends('layouts.backend', ['menu_active' => 'dashboard'])

@section('title')
	<h4><i class="icon-exclamation position-left"></i> <span class="text-semibold">Home</span> - Dashboard</h4>
	<ul class="breadcrumb breadcrumb-caret position-right">
		<li><a href="{{ route('admin.home')}}">Home</a></li>
		<li class="active">Dasrhboard</li>
	</ul>
@endsection

@section('content')
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Hi, Have a nice day ! </h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<li><a data-action="reload"></a></li>
            		<li><a data-action="close"></a></li>
            	</ul>
        	</div>
		</div>
	</div>
	<!-- /sidebars overview -->
@endsection
