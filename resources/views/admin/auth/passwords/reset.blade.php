@extends('layouts.master_layout')


@section('content')
    <div class="col-sm-offset-4 col-sm-4">
        {{ Form::open(['url' => route('admin.reset'), 'method' => 'post']) }}

            <div class="panel panel-body login-form">
                <div class="text-center">
                    <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
                    <h5 class="content-group">Reset Password <small class="display-block">Your credentials</small></h5>
                </div>

                <input name="token" type="hidden" value="{{$token}}">

                <input type="hidden" name="id" value="{{{ $admin->id }}}">
                <input type="hidden" name="date" value="{{{ $date }}}">
                <input type="hidden" name="email" value="{{{ $admin->email }}}">

                <div class="form-group has-feedback has-feedback-left {{ $errors->has('password') ? ' has-error' : '' }}"">
                    <input type="password" placeholder="New Password" class="form-control" name="password" required autofocus>
                    <div class="form-control-feedback">
                        <i class="icon-lock2 text-muted"></i>
                    </div>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group has-feedback has-feedback-left {{ $errors->has('password') ? ' has-error' : '' }}"">
                    <input type="password" placeholder="Confirm Password" class="form-control" name="password_confirm" required autofocus>
                    <div class="form-control-feedback">
                        <i class="icon-lock2 text-muted"></i>
                    </div>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <button type="submit" class="btn bg-blue btn-block">Send Password Reset Request <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection
@section('content-js')
    <script type="text/javascript" src="{{ mix('js/login.js')}}"></script>
@endsection