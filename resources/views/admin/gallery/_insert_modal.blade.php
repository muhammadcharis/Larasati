<div id="insertGalleryModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			
				<form action="{{ route('gallery.store') }}" type="post" class="form-horizontal" id="insert_gallery" enctype="multipart/form-data">
				 {{ csrf_field() }}
				<div class="modal-body">
					@include('form.select', [
						'field' => 'service',
						'label' => 'Service',
						'label_col' => 'col-xs-2',
						'form_col' => 'col-xs-10',
						'options' => $services,
						'attributes' => [
							'id' => 'service'
						]
					])
					
					@include('form.text', [
						'field' => 'name',
						'label' => 'Name',
						'label_col' => 'col-xs-2',
						'form_col' => 'col-xs-10',
						'attributes' => [
							'id' => 'name'
						]
					])
					
					@include('form.textarea', [
						'field' => 'description',
						'label' => 'Description',
						'label_col' => 'col-xs-2',
						'form_col' => 'col-xs-10',
						'attributes' => [
							'id' => 'description'
						]
					])
					
					@include('form.file', [
						'field' => 'file_upload',
						'label' => 'Image',
						'label_col' => 'col-xs-2',
						'form_col' => 'col-xs-10',
						'attributes' => [
							'id' => 'file_upload'
						]
					])

				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Submit form</button>
				</div>
			</form>
		</div>
	</div>
</div>