@extends('layouts.backend', ['menu_active' => 'gallery'])

@section('title')
	<h4><i class="icon-exclamation position-left"></i> <span class="text-semibold">Company Profile</span> -  Gallery</h4>
	<ul class="breadcrumb breadcrumb-caret position-right">
		<li><a href="{{ route('admin.home')}}">Company Profile</a></li>
		<li class="active">Gallery</li>
	</ul>
@endsection

@section('content')
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<a href="#" class="btn btn-default" data-toggle="modal" data-target="#insertGalleryModal"><i class="icon-plus2 position-left"></i>Create New</a>
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table datatable-show-all table-striped']) !!}
			</div>
		</div>
	</div>
@endsection

@section('content')
	<div class="panel panel-flat">
		<div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table datatable-show-all table-striped']) !!}
			</div>
		</div>
	</div>

@endsection

@section('modal')
	@include('admin.gallery._insert_modal')
@endsection

@section('content-js')
{!! $html->scripts() !!}
<script>

	$('#insert_gallery').submit(function (event) {
		event.preventDefault();
		bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
			if(result){
				$.ajax({
					type: "POST",
					url: $('#insert_gallery').attr('action'),
					data:new FormData($("#insert_gallery")[0]),
					processData: false,
					contentType: false,
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
						$('#insertGalleryModal').modal('hide');
					},
					complete: function () {
						$.unblockUI();
						$('#insertGalleryModal').modal('hide');
					},
					success: function (response) {
						$('#insert_gallery').trigger("reset");
						$('#dataTableBuilder').DataTable().ajax.reload();
						$("#alert_success").trigger("click", 'Data Berhasil disimpan');
					},
					error: function (response) {
						$.unblockUI();
						$('#insertGalleryModal').modal();
					}
				});
			}
		});
	});

	function hapus(url){
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		
		$.ajax({
			type: "delete",
			url: url,
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			success: function (response) {
				$.unblockUI();
			},
			error: function (response) {
				$.unblockUI();
			}
		}).done(function ($result) {
			$('#dataTableBuilder').DataTable().ajax.reload();
			$("#alert_success").trigger("click", 'Data Berhasil hapus');
		});
	}
</script>
@endsection
