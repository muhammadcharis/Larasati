<script type="x-tmpl-mustache" id="venue-table">
	{% #item %}
		<tr>
			<td>{% no %}</td>
			<td>
				<input type="text" class="form-control update-deskripsi-detail-venue" id="detail_venue_description_{% _id %}" data-id="{% _id %}" placeholder="SILAHKAN MASUKAN DETAIL PAKET DISINI" value="{% description %}" autocomplete="off"></input>
			</td>
			<td>
				<button type="button" id="btnDeleteVenue_{% _id %}" data-id="{% _id %}" class="btn btn-danger btn-delete-venue_detail"><i class="fa icon-trash"></i></button>
			</td>

		</tr>	
	{%/item %}
	<tr>
		<td></td>
		<td>
			<input type="text" class="form-control" id="detail_venue" placeholder="SILAHKAN MASUKAN DETAIL PAKET DISINI" autocomplete="off"></input>
		</td>
		<td>
			<button type="button" id="save_detail_venue" class="btn btn-success"><i class="icon-floppy-disk"></i></button>
		</td>

	</tr>
</script>