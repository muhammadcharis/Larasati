@extends('layouts.backend', ['menu_active' => 'order'])

@section('title')
	<h4><i class="icon-exclamation position-left"></i> <span class="text-semibold">Home</span> -  Order</h4>
	<ul class="breadcrumb breadcrumb-caret position-right">
		<li><a href="{{ route('admin.home')}}">Home</a></li>
		<li><a href="{{ route('order.index')}}" id="url_order_index">Order</a></li>
		<li class="active">New</li>
	</ul>
@endsection

@section('content')
	{!!
        Form::open([
            'role' => 'form',
            'url' => route('order.store'),
            'method' => 'post',
            'enctype' => 'multipart/form-data',
            'id' => 'form'
        ])
    !!}
	<div class="panel panel-flat">
		<div class="panel-body">
			<div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    @include('form.text', [
                        'label' => 'Customer Name',
                        'field' => 'customer_name',
                        'class' => 'bg-light-grey',
                        'attributes'=> [
                            'id' => 'customer_name',
                            'autocomplete' => 'off'
                        ]
                    ])
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    @include('form.text', [
                        'label' => 'Customer Email',
                        'field' => 'customer_email',
                        'class' => 'bg-light-grey',
                        'attributes'=> [
                            'id' => 'customer_email',
                            'autocomplete' => 'off'
                        ]
                    ])
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    @include('form.text', [
                        'label' => 'Customer Phone Number',
                        'field' => 'customer_phone_number',
                        'class' => 'bg-light-grey',
                        'attributes'=> [
                            'id' => 'customer_phone_number',
                            'autocomplete' => 'off'
                        ]
                    ])
                </div>
            </div>
             <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    @include('form.date', [
                        'field' => 'event_date',
                        'label' => 'Event Date',
                        'class' => 'daterange-time',
                        'placeholder' => 'dd/mm/yyyy'
                    ])
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12">
                    @include('form.text', [
                        'label' => 'Event Name',
                        'field' => 'event_name',
                        'class' => 'bg-light-grey',
                        'attributes'=> [
                            'id' => 'event_name',
                            'autocomplete' => 'off'
                        ]
                    ])
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12">
                    @include('form.text', [
                        'label' => 'Event Location',
                        'field' => 'event_location',
                        'class' => 'bg-light-grey',
                         'attributes'=> [
                            'id' => 'event_location',
                            'autocomplete' => 'off'
                        ]
                    ])
                </div>
                
            </div>
		</div>
    </div>
    <div class="panel panel-flat">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>CATEGORY</th>
							<th>DETAIL ORDER</th>
							<th>QTY</th>
                            <th>UOM</th>
							<th>PRICE</th>
							<th>ACTION</th>
						</tr>
					</thead>
					<tbody id="tbody-order">
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4"></td>
                            <td>Total Order</td>
                            <td>
                                <input type="text" class="form-control" readonly="readonly" id="total_price_all_category_format_money">
                                {!! Form::hidden('total_price_all_category',0, array('id' => 'total_price_all_category')) !!}
                            </td>
                        </tr>
                        {{--<tr>
                            <td colspan="4"></td>
                            <td>Discount</td>
                            <td>
                                <input type="text" class="form-control" id="discount">
                            </td>
                        </tr>
                        <tr>
                           <td colspan="4"></td>
                            <td>Total Order After Discount</td>
                            <td> 
                                <input type="text" class="form-control" readonly="readonly" id="total_order_all_format_money">
                                {!! Form::hidden('total_order_all',0, array('id' => 'total_order_all')) !!}
                            </td>
                        </tr>--}}
                </tfoot>
                    {!! Form::hidden('order_details','[]' , array('id' => 'order_details')) !!}
				</table>
			</div>
		</div>
    </div>
    <hr>
	<div class="form-group text-right" style="margin-top: 10px;">
		<button type="submit" class="btn btn-info btn-lg">SAVE <i class="icon-floppy-disk position-left"></i></button>
	</div>
    {!! Form::close() !!}

@endsection



@section('content-js')
@include('admin.order._order_detail')
    <script type="text/javascript" src="{{ mix('js/datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ mix('js/formatMoney.js') }}"></script>
    <script type="text/javascript" src="{{ mix('js/admin/order.js') }}"></script>

    <script type="text/javascript">
        $('.daterange-time').daterangepicker({
            timePicker: true,
            applyClass: 'bg-slate-600',
            cancelClass: 'btn-default',
            locale: {
                format: 'DD/MM/YYYY h:mm a'
            }
        });

    </script>
@endsection
