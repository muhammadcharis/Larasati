<div id="insertVenueModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			
				<form action="{{ route('venue.store') }}" type="post" class="form-horizontal" id="insert_venue" enctype="multipart/form-data">
				 {{ csrf_field() }}
				<div class="modal-body">
					@include('form.text', [
						'field' => 'name',
						'label' => 'Package Name',
						'label_col' => 'col-xs-2',
						'form_col' => 'col-xs-10',
						'attributes' => [
							'id' => 'name'
						]
					])

					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>PACKAGE DETAIL</th>
									<th>ACTION</th>
								</tr>
							</thead>
							<tbody id="tbody-venue-package">
							</tbody>
						
							{!! Form::hidden('venue_packages','[]' , array('id' => 'venue_packages')) !!}
						</table>
					</div>

				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Submit form</button>
				</div>
			</form>
		</div>
	</div>
</div>