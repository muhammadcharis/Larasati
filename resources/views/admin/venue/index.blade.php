@extends('layouts.backend', ['menu_active' => 'venue'])

@section('title')
	<h4><i class="icon-exclamation position-left"></i> <span class="text-semibold">Company Profile</span> -  Venue</h4>
	<ul class="breadcrumb breadcrumb-caret position-right">
		<li><a href="{{ route('admin.home')}}">Company Profile</a></li>
		<li class="active">Venue</li>
	</ul>
@endsection

@section('content')
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<a href="#" class="btn btn-default" data-toggle="modal" data-target="#insertVenueModal"><i class="icon-plus2 position-left"></i>Create New</a>
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table datatable-show-all table-striped']) !!}
			</div>
		</div>
	</div>
@endsection

@section('modal')
	@include('admin.venue._insert_modal')
@endsection

@section('content-js')
	@include('admin.venue._venue_detail')
	{!! $html->scripts() !!}
 	<script type="text/javascript" src="{{ mix('js/admin/venue.js') }}"></script>
@endsection
