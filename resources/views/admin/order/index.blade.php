@extends('layouts.backend', ['menu_active' => 'order'])

@section('title')
	<h4><i class="icon-exclamation position-left"></i> <span class="text-semibold">Home</span> -  Order</h4>
	<ul class="breadcrumb breadcrumb-caret position-right">
		<li><a href="{{ route('admin.home')}}">Home</a></li>
		<li class="active">Order</li>
	</ul>
@endsection

@section('content')
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<a href="{{ route('order.create') }}" class="btn btn-default"><i class="icon-plus2 position-left"></i>Create New</a>
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table datatable-show-all table-striped']) !!}
			</div>
		</div>
	</div>

	{!! Form::hidden('flag_msg',$flag , array('id' => 'flag_msg')) !!}
	{!! Form::hidden('order_details','[]' , array('id' => 'order_details')) !!}
@endsection

@section('content-js')
	{!! $html->scripts() !!}
	<script type="text/javascript" src="{{ mix('js/formatMoney.js') }}"></script>
 	<script type="text/javascript" src="{{ mix('js/admin/order.js') }}"></script>
@endsection
