<script type="x-tmpl-mustache" id="order-table">
	{% #item %}
		<tr>
			<td>{% no %}</td>
			<td>
				{% category %}
			</td>
			<td></td>
			<td>
				{% qty_kategori %}
			</td>
			<td>
				{% uom_kategori %}
			</td>
			<td>
				<input type="text" class="form-control input-number update-total-harga" id="updateTotalHarga_{% _id %}" value="{% total_price_format_money %}" data-id="{% _id %}" autocomplete="off">
			</td>
			<td>
				<button type="button" id="btnUpdateTotalHarga_{% _id %}" class="hidden btn-update-total-harga" data-id="{% _id %}"></button>
				<button type="button" id="btndeleteCetegory_{% _id %}" data-id="{% _id %}" class="btn btn-danger btn-delete-cetegory"><i class="fa icon-trash"></i></button>
			</td>

		</tr>	
		{% #flag_set %}
			{% #details %}	
				<tr>
					<td colspan="6">
						<textarea class="form-control update-deskripsi-detail-order" id="detail_order_input_{% _id %}_{% _idx %}" data-id="{% _id %}" data-idx="{% _idx %}" placeholder="SILAHKAN MASUKAN DETAIL ORDER DISINI" style="overflow:auto;resize:none" rows="5" cols="10" autocomplete="off">{% detail_order %}</textarea>
					</td>
					<td>
						<button type="button" id="btndeleteDetailOrder_{% _id %}_{% _idx %}" data-id="{% _id %}" data-idx="{% _idx %}" class="btn btn-danger btn-delete-detail-order"><i class="fa icon-trash"></i></button>
					</td>
				</tr>
			{% /details %}
			<tr>
				<td colspan="6">
					<textarea class="form-control input-new-detail-order" id="detail_order_{% _id %}" data-id="{% _id %}" placeholder="SILAHKAN MASUKAN DETAIL ORDER DISINI" style="overflow:auto;resize:none" rows="5" cols="10" autocomplete="off"></textarea>
				</td>
				<td>
					<button type="button" id="save_detail_order_{% _id %}" data-id="{% _id %}" class="btn btn-success btn-save-detail-order"><i class="icon-floppy-disk"></i></button>
				</td>
			</tr>
		{% /flag_set %}
		{% ^flag_set %}
			{% #details %}	
				<tr>
					<td colspan="3">
						<textarea class="form-control update-deskripsi-detail-order" id="detail_order_input_{% _id %}_{% _idx %}" data-id="{% _id %}" data-idx="{% _idx %}" placeholder="SILAHKAN MASUKAN DETAIL ORDER DISINI" style="overflow:auto;resize:none" rows="5" cols="10" autocomplete="off">{% detail_order %}</textarea>
					</td>
					<td><input type="text" class="form-control input-number update-qty-detail" id="updateQty_{% _id %}_{% _idx %}" value="{% qty %}" data-id="{% _id %}" data-idx="{% _idx %}" autocomplete="off"></td>
					<td>{% uom %}</td>
					<td><input type="text" class="form-control input-number update-harga-satuan" id="updateHargaSatuan_{% _id %}_{% _idx %}" value="{% harga_satuan_format_money %}" data-id="{% _id %}" data-idx="{% _idx %}" autocomplete="off"></td>
					<td>
						<button type="button" id="btnUpdateTotalHargaSatuan_{% _id %}_{% _idx %}" class="hidden btn-update-total-harga-satuan" data-id="{% _id %}" data-idx="{% _idx %}"></button>
						<button type="button" id="btndeleteDetailOrder_{% _id %}_{% _idx %}" data-id="{% _id %}" data-idx="{% _idx %}" class="btn btn-danger btn-delete-detail-order"><i class="fa icon-trash"></i></button>
					</td>
				</tr>
			{% /details %}
			<tr>
				
				<td colspan="3">
					<textarea class="form-control input-new-detail-order" id="detail_order_{% _id %}" data-id="{% _id %}" placeholder="SILAHKAN MASUKAN DETAIL ORDER DISINI" style="overflow:auto;resize:none" rows="5" cols="10" autocomplete="off"></textarea>
				</td>
				<td>
					<input type="text" class="form-control input-number" id="inputQty_{% _id %}" data-id="{% _id %}" autocomplete="off">
				</td>
				<td>
					<input type="text" class="form-control" id="inputUom_{% _id %}" data-id="{% _id %}" autocomplete="off">
				</td>
				<td>
					<input type="text" class="form-control input-number input-harga-satuan" id="hargaSatuanInput_{% _id %}" data-id="{% _id %}" autocomplete="off">
				</td>
				<td>
					<button type="button" id="save_detail_order_{% _id %}" data-id="{% _id %}" class="btn btn-success btn-save-detail-order"><i class="icon-floppy-disk"></i></button>
				</td>
			</tr>
		{% /flag_set %}
		
	{%/item %}
	<tr>
		<td>

		</td>
		<td>
			<div class="col-md-8 col-lg-8 col-sm-12">
				<select name="category_option" id="category_option" class="form-control">
					<option value="SEWA_GEDUNG">SEWA GEDUNG</option>
					<option value="DEKORASI">DEKORASI</option>
					<option value="RIAS">RIAS</option>
					<option value="WEDDING_ORGANIZER">WEDDING ORGANIZER</option>
					<option value="OTHER">OTHER</option>
				</select>
			</div>
		</td>
		<td></td>
		<td></td>
		<td></td>
		<td>
			<input type="text" class="form-control input-number input-kategori" id="harga">
		</td>
		<td>
			<button type="button" id="selected_category" class="btn btn-success"><i class="icon-floppy-disk"></i></button>
		</td>

	</tr>
</script>