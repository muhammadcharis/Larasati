<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModal">
        <div class="modal-dialog" role="document">
		{{ Form::open(['method' => 'PUT', 'id' => 'updateformnya', 'class' => 'form-horizontal', 'url' => '#']) }}
		    <div class="modal-content">
                <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">PERMISSION FORM</h5>
				</div>
				<div class="modal-body">
				   <div class="row">  
					   <div class="col-md-12">
						    @include('form.text', [
								'field' => 'name',
								'label' => 'NAME',
								'placeholder' => 'Permission Name',
								'attributes' => [
									'id' => 'update_name'
								]
							])
							@include('form.textarea', [
								'field' => 'description',
								'label' => 'DESCRIPTION',
								'placeholder' => 'Description',
								'attributes' => [
									'id' => 'update_description'
								]
							])
							{!! Form::hidden('id','', array('id' => 'update_id')) !!}
						</div>
					</div>
				</div>
                <div class="modal-footer text-center">
					<button type="submit" class="btn btn-success">Save</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                
			</div>
		{{ Form::close() }}
        </div>
	</div>