<div id="insertTimelineModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			
				<form action="{{ route('timeline.store') }}" type="post" class="form-horizontal" id="insert_timeline" enctype="multipart/form-data">
				 {{ csrf_field() }}
				<div class="modal-body">
					@include('form.textarea', [
						'field' => 'description',
						'label' => 'Description',
						'label_col' => 'col-xs-2',
						'form_col' => 'col-xs-10',
						'attributes' => [
							'id' => 'description'
						]
					])

					@include('form.date', [
						'field' => 'timeline_date',
						'label' => 'Timeline Date',
						'label_col' => 'col-xs-2',
						'form_col' => 'col-xs-10',
						'class' => 'single-date',
						'placeholder' => 'dd/mm/yyyy',
						'attributes' => [
							'id' => 'timeline_date',
							'autocomplete' => 'off'
						]
					])
					
					@include('form.file', [
						'field' => 'file_upload',
						'label' => 'Image',
						'label_col' => 'col-xs-2',
						'form_col' => 'col-xs-10',
						'attributes' => [
							'id' => 'file_upload'
						]
					])

				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Submit form</button>
				</div>
			</form>
		</div>
	</div>
</div>