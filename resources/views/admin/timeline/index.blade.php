@extends('layouts.backend', ['menu_active' => 'timeline'])

@section('title')
	<h4><i class="icon-exclamation position-left"></i> <span class="text-semibold">Company Profile</span> -  Timeline</h4>
	<ul class="breadcrumb breadcrumb-caret position-right">
		<li><a href="{{ route('admin.home')}}">Company Profile</a></li>
		<li class="active">Timeline</li>
	</ul>
@endsection

@section('content')
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<a href="#" class="btn btn-default" data-toggle="modal" data-target="#insertTimelineModal"><i class="icon-plus2 position-left"></i>Create New</a>
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table datatable-show-all table-striped']) !!}
			</div>
		</div>
	</div>
@endsection

@section('modal')
	@include('admin.timeline._insert_modal')
@endsection

@section('content-js')
{!! $html->scripts() !!}
<script>

	$('#insert_timeline').submit(function (event) {
		event.preventDefault();
		bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
			if(result){
				$.ajax({
					type: "POST",
					url: $('#insert_timeline').attr('action'),
					data:new FormData($("#insert_timeline")[0]),
					processData: false,
					contentType: false,
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
						$('#insertTimelineModal').modal('hide');
					},
					complete: function () {
						$.unblockUI();
						$('#insertTimelineModal').modal('hide');
					},
					success: function (response) {
						$('#insert_timeline').trigger("reset");
						$('#dataTableBuilder').DataTable().ajax.reload();
						$("#alert_success").trigger("click", 'Data Berhasil disimpan');
					},
					error: function (response) {
						$.unblockUI();
						$('#insertTimelineModal').modal();
					}
				});
			}
		});
	});

	function hapus(url){
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		
		$.ajax({
			type: "delete",
			url: url,
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			success: function (response) {
				$.unblockUI();
			},
			error: function (response) {
				$.unblockUI();
			}
		}).done(function ($result) {
			$('#dataTableBuilder').DataTable().ajax.reload();
			$("#alert_success").trigger("click", 'Data Berhasil hapus');
		});
	}

	$('.single-date').datepicker({ 
        format: "dd/mm/yyyy",
	    autoclose: true,
	    todayHighlight: true
    });
	$('.pickadate').pickadate();
</script>
@endsection
