
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($show))
                <li><a href="{!! $show !!}"><i class="icon-search4"></i> Details</a></li>
            @endif

            @if (isset($edit))
                <li><a href="{!! $edit !!}"><i class="icon-pencil6"></i> Edit</a></li>
            @endif

            @if (isset($edit_modal))
                <li><a href="#" onclick="edit('{!! $edit_modal !!}')" ><i class="icon-pencil6"></i> Edit</a></li>
            @endif

            @if (isset($approve))
                <li><a href="#" onclick="approve('{!! $approve !!}')"><i class="icon-file-check2"></i> Approve</a></li>
            @endif

            @if (isset($reject))
                <li><a href="#" onclick="reject('{!! $reject !!}')"><i class=" icon-x"></i> Reject</a></li>
            @endif

            @if (isset($delete))
                <li><a href="#" onclick="hapus('{!! $delete !!}')"><i class="icon-close2"></i> Delete</a></li>
            @endif

            @if (isset($print))
                <li><a href="{!! $print !!}" target="_blank"><i class="icon-printer2"></i> Print</a></li>
            @endif

            @if (isset($printBarcodeAndExcel))
                <li><a href="#" onclick="printBarcodeAndExcel('{!! $printBarcodeAndExcel !!}')"><i class="icon-printer2"></i> Print</a></li>
            @endif
        </ul>
    </li>
</ul>