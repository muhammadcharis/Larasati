@extends('layouts.frontend',
            [
                'title'=>'Tentang Kami',
                'active'=>'about',
                'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
            ]
        )

@section('content-css')
    <link href="{{ mix('css/animate.css') }}" rel="stylesheet">

<style type="text/css">
</style>
@endsection

@section('content')
    @include('includes._page-header',['ptitle' => 'Tentang Kami','bgimg'=>'/images/bouquet.jpg'])  


    <section id="top-about">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p><strong>Larasati</strong> merupakan penyedia jasa pernikahan yang berdiri sejak tahun 1992. 
                        Kami melayani jasa pernikahan mulai dari venue, dekorasi, rias pengantin, dan keperluan pelengkap lainnya. 
                        Dengan pengalaman lebih dari 20 tahun, kami selalu berusaha melakukan inovasi untuk memenuhi kebutuhan dan keinginan konsumen. Bagi kami, kepuasan konsumen adalah hal yang utama.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="">
        <div class="container works-head">
            <div class="row-fluid">
                <div class="col-md-8 col-md-offset-2 text-center main-title">
                    <h3 class="text-center no-m">Kami juga mempunyai perjalan sama seperti kalian.<br>Dan inilah perjalan kami</h3>
                </div>
            </div>
        </div>
    </section>
    <section class="works-section relative" id="animate-top-sec">
        <div class="container">
            <div class="row relative">
                <div class="vertical-line delay-2" id="animate-top-line"></div>
                <div class="small-circle-top delay-1" id="animate-top-circle"></div>
                <p>&nbsp<br>&nbsp<br>&nbsp<br></p>
            </div>
        </div>
    </section>

   
    <about-us-component></about-us-component>

   {{-- @foreach ($timelines as $key => $value)
       <section class="works-section relative" id="animate-sec-{{ $key+1 }}">
            <div class="{{ (($key+1) % 2 == 0) ? ' gradient-right' : ' gradient-left' }}" id="animate-background-{{ $key+1 }} delay-5">
                <img src="{{ route('aboutImage',$value->filename) }}" height="100%" width="100%" id="animate-image-{{ $key+1 }}" class="delay-4">
                <div class="container">
                    <div class="row relative">
                            <div class="number-inside-circle delay-2" id="animate-number-{{ $key+1 }}">{{ substr($value->timeline_date->format('Y'),0,2) }}<br/>{{ $value->timeline_date->format('y') }}</div>
                            <div class="vertical-line delay-1" id="animate-line-{{ $key+1 }}"></div>

                        <div class="content-for-works col-sm-6 delay-3 {{(($key+1) % 2 == 0) ? ' text-left col-sm-offset-6' : ' text-right'}}" id="animate-content-{{ $key+1 }}">
                            <p>
                                {{ $value->description }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endforeach --}}
    


    <section class="works-section relative" id="animate-bottom-sec-1">
        <div class="container">
            <div class="row relative">
                <div class="vertical-line delay-1" id="animate-bottom-line-1"></div>
                <p class="text-center">
                    <br>
                </p>
            </div>
        </div>
    </section>
    <section class="works-section relative" id="animate-bottom-sec">
        <div class="container">
            <div class="row relative">
                <div class="vertical-line delay-1" id="animate-bottom-line"></div>
                <div class="small-circle-bottom delay-2" id="animate-bottom-circle"></div>
                <p>&nbsp<br>&nbsp<br>&nbsp<br></p>
            </div>
        </div>
    </section>
    <section class="works-section relative" id="animate-bottom-sec-2">
        <div class="container">
            <div class="row  ptb-50">
                <div class="col-md-8 col-md-offset-2 text-center main-title">
                    <h3 class="text-center no-m">Kami akan terus maju kedepan<br> untuk memberikan layanan terbaik.</h3>
                </div>
            </div>
        </div>
    </section>

    @include('includes._askus')  

@endsection

@section('modal') 
@endsection

@section('content-js')
<script src="{{ mix('js/jquery.waypoints.min.js') }}"></script>
<script src="{{ mix('js/about.js') }}"></script>
<script type="text/javascript">
</script> 
@endsection


