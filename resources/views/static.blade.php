@extends('layouts.frontend',
            [
                'title'=>'Judul Static Page',
                'active'=>'',
                'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
            ]
        )

@section('content-css')
<style type="text/css">
</style>
@endsection

@section('content')
     @include('includes._page-header',['ptitle' => 'Judul Static Page','bgimg'=>'/images/bonquet.jpg'])  

    <section class="bluelight">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="active">Judul Static Page</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section id="homeBlog">
        <div class="container detail-blog">
            <div class="row">
                <div class="col-xs-12 main-title">
                    <h1 class="text-center">Judul Static Page</h1>
                    <p class="col-md-6 col-md-offset-3 text-center">Sub Judul Static Page</p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </p>
                </div>
            </div>
        </div>
    </section>

    @include('includes._askus',['askcolor' => 'peach'])  

@endsection

@section('modal') 
@endsection

@section('content-js') 
@endsection


