<footer>
    <section class="top-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="img-box">
                        <img src="{{ asset('images/logo.png')}}" style="width:80%">
                    </div>
                    <p></p>
                    <p><i class="fas fa-map-marker-alt"></i><span> Jl. T M Pahlawan No. 4, Kabupaten Blora, Jawa Tengah.</span></p>
                    <p><i class="fas fa-phone"></i><span> (+62296) 532074</span></p>
                    <p><i class="far fa-envelope"></i> <span>larasatiblora@gmail.com</span></p>
                    <p><a style="color:white" href="https://www.instagram.com/larasatidekor_blora/" title="instagram" target="_blank"><i class="fab fa-instagram"></i> <span>Larasati Blora Decoration</span></a></p>
                </div>
                <div class="col-sm-6">
                    <div class="map-footer">
                        <iframe
                          width="100%"
                          height="100%"
                          frameborder="0" style="border:0"
                          src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15841.734188066837!2d111.4148995!3d-6.9580757!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x15944faddd2e945d!2sGraha+Larasati!5e0!3m2!1sen!2sid!4v1549977961843" allowfullscreen>
                        </iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bottom-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    Designed by <a href="#">Larasati Blora</a> &copy; 2018 
                </div>
                {{--<div class="col-md-8 col-sm-6 text-right">
                    <a href="#">Sitemap</a>
                    <span> | </span>
                    <a href="#">Terms & Condition</a>
                    <span> | </span>
                    <a href="#">FAQ</a>
                </div>--}}
            </div>
        </div>
    </section>
</footer>