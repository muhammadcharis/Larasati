<section id="navbar-main">
    <!-- Fixed navbar -->
    <nav class="navbar navbar-fixed-top" id="mainNavbar">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{route('index')}}"><img src="{{ asset('images/icon_larasati.png')}}" height="100%"></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse pull-right">
                <ul class="nav navbar-nav">
                    <li class="{{(isset($active) && $active == 'home') ? ' active' : ''}}"><a href="{{route('index')}}">Beranda</a></li>
                    <li class="{{(isset($active) && $active == 'about') ? ' active' : ''}}"><a href="{{route('about')}}">Tentang Kami</a></li>
                    <li class="{{(isset($active) && $active == 'gallery') ? ' active' : ''}}"><a href="{{route('gallery')}}">Galeri</a></li>
                    <li class="{{(isset($active) && $active == 'venue') ? ' active' : ''}}"><a href="{{route('venue')}}">Gedung</a></li>
                    {{--<li class="{{(isset($active) && $active == 'blog') ? ' active' : ''}}"><a href="{{route('blog')}}">Blog</a></li>--}}
                    <li class="{{(isset($active) && $active == 'contact') ? ' active' : ''}}"><a href="{{route('contact')}}">Kontak</a></li>
                    <li id="appointment-nav-btn">
                        <btn-appoitment-component @button_clicked="lunchModal()"></btn-appoitment-component>
                    </li>
                    {{-- SEARCH ICON work exc on mobile--}}
                    {{-- <li><a href="#search" class="search-form-tigger btn hidden-xs"  data-toggle="search-form"><i class="glyphicon glyphicon-search" aria-hidden="true"></i></a></li> --}}
                    {{-- SEARCH ICON --}}

                    {{-- SEARCH FORM for mobile only --}}
                        {{-- <form class="col-sm-6 hidden-sm hidden-md hidden-lg hidden-xl" method="post" action="">
                            {{ csrf_field() }}
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search" name="search" value="{{ isset($search) ? $search : '' }}">
                                <div class="input-group-btn">
                                        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                </div>  
                            </div>
                        </form> --}}
                    {{-- END SEARCH FORM --}}

                </ul>
            </div><!--/.nav-collapse -->
            {{-- search nav --}}
            <div class="search-form-wrapper">
                <form class="search-form" id="" action="" method="post">
                {{ csrf_field() }}
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search" name="search" value="{{ isset($search) ? $search : '' }}">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
                </form>
            </div>
            {{-- end search nav --}}
        </div>
    </nav>
</section>