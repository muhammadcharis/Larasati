
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($edit_modal))
                <li><a href="#" onclick="edit('{!! $edit_modal !!}')" ><i class="icon-pencil6"></i> Edit</a></li>
            @endif

            @if (isset($delete))
                <li><a href="#" onclick="hapus('{!! $delete !!}')"><i class="icon-close2"></i> Delete</a></li>
            @endif

            @if (isset($print))
                <li><a href="{!! $print !!}" target="_blank"><i class="icon-printer2"></i> Print</a></li>
            @endif

            </ul>
    </li>
</ul>