<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			@yield('title')
		</div>
	</div>
</div>