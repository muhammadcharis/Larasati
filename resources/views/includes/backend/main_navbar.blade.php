<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="{{ route('admin.home')}}"><img src="{{ asset('images/logo.png') }}" alt=""></a>

			<ul class="nav navbar-nav pull-right visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<span>{{ ucwords(Auth::guard('admins')->user()->name) }}</span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>
						<li><a href="{{ route('admin.do_logout') }}"
							onclick="event.preventDefault();
									document.getElementById('logout-form').submit();">
							<i class="icon-switch2"></i> Logout</a>
							<form id="logout-form" action="{{ route('admin.do_logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
