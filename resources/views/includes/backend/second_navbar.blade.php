<div class="navbar navbar-default" id="navbar-second">
		<ul class="nav navbar-nav no-border visible-xs-block">
			<li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="icon-menu7"></i></a></li>
		</ul>

		<div class="navbar-collapse collapse" id="navbar-second-toggle">
			<ul class="nav navbar-nav">
				<li class="{{ isset($active) && $active == 'dashboard' ? 'active' : '' }}"><a href="{{ route('admin.home') }}"><i class="icon-display4 position-left"></i> Dashboard</a></li>
			
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-make-group position-left"></i> Company Profile <span class="caret"></span>
					</a>

					<ul class="dropdown-menu width-250">
						<li class="{{ isset($active) && $active == 'slider' ? 'active' : '' }}" ><a href="{{ route('slider.index') }}"><i class="icon-images2"></i> Slider</a></li>
						<li class="{{ isset($active) && $active == 'timeline' ? 'active' : '' }}" ><a href="{{ route('timeline.index') }}"><i class="icon-magazine"></i> Timeline</a></li>
						<li class="{{ isset($active) && $active == 'gallery' ? 'active' : '' }}" ><a href="{{ route('gallery.index') }}"><i class="icon-gallery"></i> Gallery</a></li>
						<li class="{{ isset($active) && $active == 'venue' ? 'active' : '' }}" ><a href="{{ route('venue.index') }}"><i class="icon-city"></i> <span>Venue</span></a></li>
						<li class="{{ isset($active) && $active == 'service' ? 'active' : '' }}" ><a href="{{ route('service.index') }}"><i class="icon-stack3"></i> <span>Service</span></a></li>
						<li class="{{ isset($active) && $active == 'blog' ? 'active' : '' }}" ><a href="{{ route('blog.index') }}"><i class="icon-blog"></i> <span>Blog</span></a></li>
						<li class="{{ isset($active) && $active == 'partner' ? 'active' : '' }}" ><a href="{{ route('partner.index') }}"><i class="icon-users4"></i> <span>Partner</span></a></li>
						<li class="{{ isset($active) && $active == 'partner' ? 'active' : '' }}" ><a href="https://api.instagram.com/oauth/authorize/?client_id=d4d99e2759104825999fe372b378aada&redirect_uri=http://larasatiblora.com/instagram/callback&response_type=code" target='_blank'><i class="icon-users4"></i> <span>IG API</span></a></li>

					</ul>
				</li>

			
				<li class="{{ isset($active) && $active == 'order' ? 'active' : '' }}" ><a href="{{ route('order.index') }}"><i class="icon-file-plus"></i> Order</a></li>
						

				
			</ul>

		</div>
	</div>
