<section id="pg-header"  class="parallax" 
style="background-image: url('{{(isset($bgimg) && $bgimg != '') ? $bgimg : ''}}');">
    <div class="parralax-overlay"></div>
    <div class="container">
        <div class="pg-header-space"></div>

        <div class="row pg-header-content">
            <div class="col-xs-12 text-center">
                <h1>{{(isset($ptitle) && $ptitle != '') ? $ptitle : ''}}</h1>
            </div>
        </div>
    </div>
</section>