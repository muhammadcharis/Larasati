<section id="askus" class="{{(isset($askcolor) && $askcolor != '') ? $askcolor : ''}}">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="ask-text">
                    <h3>
                        Ingin menjadikan hari istimewa Anda lebih istimewa? Jangan ragu untuk bertanya
                    </h3>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="ask-btn">
                    <div class="ask-btn-v">
                       <btn-appoitment-component @button_clicked="lunchModal()"></btn-appoitment-component>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>