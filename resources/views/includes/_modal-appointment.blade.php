<div class="modal fade" id="modal-appointment">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fas fa-times"></i></button>
                <h4 class="modal-title"><strong class="text-red">Make Appointment</strong></h4>
            </div>
            {!!
                Form::open([
                    'role' => 'form',
                    'url' => '#',
                    'id' => 'formAppointment',
                    'method' => 'post',
                    'enctype' => 'multipart/form-data'
                ])
            !!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <p class="text-center">
                            We will hear you, and make your special day more special, just fill form bellow, and we will contact you soon.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        
                        
                            @include('form.text', [
                                'field' => 'name',
                                'class' => 'mf-name',
                                'placeholder'=>'Name',
                            ])
                            @include('form.text', [
                                'field' => 'email',
                                'class' => 'mf-email',
                                'placeholder'=>'Email',
                            ])
                            @include('form.text', [
                                'field' => 'telp',
                                'class' => 'mf-telp',
                                'placeholder'=>'Telp.',
                            ])

                            @include('form.date', [
                                'field' => 'date',
                                'class' => 'pickadate mf-date',
                                'placeholder' => 'dd/mm/yyyy',
                                'attributes' => [
                                    'autocomplete'=>'off',
                                    'id'=>'appointment_date'
                                ]
                            ])

                            

                            @include('form.select', [
                                'class' => 'mf-select',
                                'field' => 'topik',
                                'options' => [
                                    '0' => '- Select Service -',
                                    'Decor' => 'Service Decor',
                                    'Venue' => 'Venue',
                                ],
                                'default' => 0,
                            ])
                            @include('form.textarea', [
                                'field' => 'message',
                                'class' => 'mf-textarea',
                                'rows' => '5',
                                'placeholder'=>'Tell us your story and what you need here',
                            ])

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="notif-success" type="submit" class="btn btn-appointment mf-btn-submit"><strong>Submit Request</strong></button>
           
            </div>
         {!! Form::close() !!}
        </div>
    </div>
</div>

