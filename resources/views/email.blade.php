<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head>
<!--[if gte mso 9]><xml>
<o:OfficeDocumentSettings>
<o:AllowPNG/>
<o:PixelsPerInch>96</o:PixelsPerInch>
</o:OfficeDocumentSettings>
</xml><![endif]-->
<title>Larasati</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0 ">
<meta name="format-detection" content="telephone=no">
<!--[if !mso]><!-->
<link href="https://fonts.googleapis.com/css?family=Dancing+Script:400,700|Raleway:300,400,500,700,900" rel="stylesheet"><!--<![endif]-->
<style type="text/css">
    body {
        margin: 0 !important;
        padding: 0 !important;
        -webkit-text-size-adjust: 100% !important;
        -ms-text-size-adjust: 100% !important;
        -webkit-font-smoothing: antialiased !important;
    }
    img {
        border: 0 !important;
        outline: none !important;
    }
    p {
        margin: 0px !important;
        padding: 0px !important;
    }
    table {
        border-collapse: collapse;
        mso-table-lspace: 0px;
        mso-table-rspace: 0px;
    }
    table td,table th{
        padding: 10px 20px;
    }
    td, a, span {
        border-collapse: collapse;
        mso-line-height-rule: exactly;
    }
    .em-mail-table{ 
        box-shadow: 0px 0px 20px #FACCD4;
    }

    .btn {
        display: inline-block;
        margin-bottom: 0;
        font-weight: normal;
        text-align: center;
        vertical-align: middle;
        -ms-touch-action: manipulation;
        touch-action: manipulation;
        cursor: pointer;
        background-image: none;
        border: 1px solid transparent;
        white-space: nowrap;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.6;
        border-radius: 4px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    .btn-appointment {
        color: #fff;
        font-weight: normal;
        background-color: #E43137;
    }
    @media only screen and (min-width:481px) and (max-width:699px) {
    }
    @media screen and (max-width: 480px) {
    }
</style>
</head>

<body class="em_body" style="margin:0px; padding:0px;" bgcolor="#fff">

    <table width="100%">
        <tbody>
            <tr>
                <td valign="top" align="center">
                    <table class="em-mail-table" width="700px">
                        <tr>
                            <td width="50%" align="left" style="background-color: #FFEBE3;">
                                <p>
                                    If you can’t see this email?
                                </p>
                            </td>
                            <td class="red" width="50%" align="right" style="background-color: #FFEBE3;">
                                <a href="#" target="_blank" style="text-decoration:none;">View it in your browser.</a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center" style="background-color: #EDF2F4">
                                <!-- tolong ubah dengan image dengan full domain (httpnya, jangan url dari laravel) -->
                                <img src="https://www.griyapengantinhusna.com/wp-content/uploads/2017/10/rias-nikah.png" height="250px;" style="margin-top: 60px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" valign="center"  style="background-color: #EDF2F4">
                                <h1 style="text-align: center;font-family: 'Dancing Script', cursive; padding: 0px 40px;">
                                    "Terima kasih sudah menggunakan Larasati sebagai wedding dekorasi dan selamat menempuh hidup baru."
                                    
                                </h1>
                                {{--<div style="text-align: center">
                                    <a class="btn btn-appointment">Berikan Kesan</a>
                                </div>--}}
                                
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="background-color: #313536;">
                                <!-- tolong ubah dengan image dengan full domain (httpnya, jangan url dari laravel) -->
                                <img src="{{ asset('images/logo.png')}}" height="80px">
                            </td>
                            <td valign="center" align="left" style="background-color: #313536;">
                                <p style="color: #fff">
                                    Jl. Selekor, Patemon Gunungpati, Semarang ID <br>
                                    0821 4545 4080 <br>
                                    larasati@gmail.com
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center" style="background-color: #1E2424;color: #fff">
                                &copy; 2018 , All Right Reserved, Larasati Decor <br>
                                <p style="font-size: 10px;margin-top: 5px !important;">
                                    <a href="#" style="color:#fff; text-decoration:none;">Official Site</a>
                                    <span> | </span>
                                    <a href="#" style="color:#fff; text-decoration:none;">Terms & Condition</a>
                                    <span> | </span>
                                    <a href="#" style="color:#fff; text-decoration:none;">FAQ</a>
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
<div class="em_hide" style="white-space: nowrap; display: none; font-size:0px; line-height:0px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
</body></html>