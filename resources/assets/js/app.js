
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('btn-appoitment-component', {
    template: `<a class="btn btn-appointment text-uppercase" @click="lunch()">buat janji</a>`,
    methods: {
        lunch() {
           this.$emit('button_clicked');
        }
    }
});

Vue.component('modal-appoitment-component', require('./components/ModalAppoitmentComponent.vue'));
Vue.component('package-venue-component', require('./components/PackageVenueComponent.vue'));
Vue.component('contact-component', require('./components/ContactComponent.vue'));
Vue.component('service-component', require('./components/ServiceComponent.vue'));
Vue.component('about-us-component', require('./components/AboutUsComponent.vue'));
Vue.component('slider-component', require('./components/SliderComponent.vue'));
Vue.component('slider-venue-component', require('./components/SliderVenueComponent.vue'));
Vue.component('gallery-service-component', require('./components/GalleryServiceComponent.vue'));
Vue.component('instagram-feed-component', require('./components/InstagramComponent.vue'));
/*Vue.component('gallery-component', require('./components/GalleryComponent.vue'));*/

const app = new Vue({
    el: '#app',
    methods: {
        lunchModal(){
            $('#myModal').modal('show');
        }
    }
});
