//serach
$( document ).ready(function() {
    $('[data-toggle=search-form]').click(function() {
        $('.search-form-wrapper').toggleClass('open');
        $('.search-form-wrapper .search').focus();
        $('html').toggleClass('search-form-open');
        $('.search-form').css('transition','opacity 0.5s ease')
        // $("html, body").animate({scrollTop: 0}, 1000);
    });
    $('[data-toggle=search-form-close]').click(function() {
        $('.search-form-wrapper').removeClass('open');
        $('html').removeClass('search-form-open');
    });
    $('.search-form-wrapper .search').keypress(function( event ) {
       if($(this).val() == "Search") $(this).val("");
    });

    $('.search-close').click(function(event) {
       $('.search-form-wrapper').removeClass('open');
       $('html').removeClass('search-form-open');
    });
});
//end search

//scroll navbar
$(window).scroll(function() {
    var nav = $('#mainNavbar');
    var top = 200;
    if ($(window).scrollTop() >= top) {

        nav.addClass('navbar-color');
        nav.css('transition','background-color 0.5s ease-in');

    } else {
        nav.removeClass('navbar-color');
        nav.css('transition','background-color 0.5s ease-out');
    }
});
//end scroll navbar
// modal appointment
$(function(){
    $('.pickadate').datepicker({ 
        format: "dd M yyyy",
        autoclose: true,
        todayHighlight: true,
    });

    var n = nod();

    // We disable the submit button if there are errors.
    n.configure({
        // Adding a custom success message (will be shown for every
        // field).
        successMessage: 'Looks Nice,',

        // Adding our own classes.
        successClass: 'has-success',
        successMessageClass: 'text-success',
        errorClass: 'has-error',
        errorMessageClass: 'text-danger',

        submit: '.mf-btn-submit',
        disableSubmit: true
    });

    n.add([{
        selector: '.mf-name',
        validate: 'presence',
        errorMessage: 'Your name must be fill.'
    }, {
        selector: '.mf-email',
        validate: 'email',
        errorMessage: 'That doesn\'t look like it\'s a valid email address.'
    }, {
        selector: '.mf-telp',
        validate: 'float',
        errorMessage: 'That doesn\'t look like it\'s your phone number.'
    }, {
        selector: '.mf-date',
        validate: 'presence',
        errorMessage: 'Please Pick a date.'
    }, {
        selector: '.mf-select',
        validate: 'not:0',
        errorMessage: 'Talk about Venue or decoration?. pick it.'
    }, {
        selector: '.mf-textarea',
        validate: 'min-length:10',
        errorMessage: 'At least you need 10 character to explain what you need.'
    }]); 



});
// end modal