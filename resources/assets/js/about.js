$(document).ready(function(){  
    $('#animate-sec-1').waypoint(function() {
        $('#animate-sec-1').addClass('animated fadeIn');
        $('#animate-line-1').addClass('animated fadeIn');
        $('#animate-number-1').addClass('animated fadeIn');
        $('#animate-image-1').addClass('animated fadeIn');
        $('#animate-content-1').addClass('animated fadeIn');
        $('#animate-background-1').addClass('animated fadeIn');
        $('.works-section').css('z-index','unset');
    }, { offset: '50%' });
    $('#animate-sec-2').waypoint(function() {
        $('#animate-sec-2').addClass('animated fadeIn');
        $('#animate-line-2').addClass('animated fadeIn');
        $('#animate-number-2').addClass('animated fadeIn');
        $('#animate-image-2').addClass('animated fadeIn');
        $('#animate-content-2').addClass('animated fadeIn');
        $('#animate-background-2').addClass('animated fadeIn');
        $('.works-section').css('z-index','unset');
    }, { offset: '50%' });
    $('#animate-sec-3').waypoint(function() {
        $('#animate-sec-3').addClass('animated fadeIn');
        $('#animate-line-3').addClass('animated fadeIn');
        $('#animate-number-3').addClass('animated fadeIn');
        $('#animate-image-3').addClass('animated fadeIn');
        $('#animate-content-3').addClass('animated fadeIn');
        $('#animate-background-3').addClass('animated fadeIn');
        $('.works-section').css('z-index','unset');
    }, { offset: '50%' });
    $('#animate-sec-4').waypoint(function() {
        $('#animate-sec-4').addClass('animated fadeIn');
        $('#animate-line-4').addClass('animated fadeIn');
        $('#animate-number-4').addClass('animated fadeIn');
        $('#animate-image-4').addClass('animated fadeIn');
        $('#animate-content-4').addClass('animated fadeIn');
        $('#animate-background-4').addClass('animated fadeIn');
        $('.works-section').css('z-index','unset');
    }, { offset: '50%' });

    // for top line
    $('#animate-top-sec').waypoint(function() {
        $('#animate-top-sec').addClass('animated fadeIn');
        $('#animate-top-circle').addClass('animated fadeIn');
        $('#animate-top-line').addClass('animated fadeIn');
    }, { offset: '50%' });
    $('#animate-bottom-sec').waypoint(function() {
        $('#animate-bottom-sec').addClass('animated fadeIn');
        $('#animate-bottom-circle').addClass('animated fadeIn');
        $('#animate-bottom-line').addClass('animated fadeIn');
    }, { offset: '50%' });
    
    //for bottom line 
    $('#animate-bottom-sec-1').waypoint(function() {
        $('#animate-bottom-sec-1').addClass('animated fadeIn');
        $('#animate-bottom-line-1').addClass('animated fadeIn');
    }, { offset: '80%' });
    $('#animate-bottom-sec-2').waypoint(function() {
        $('#animate-bottom-sec-2').addClass('animated fadeIn');
        $('#animate-bottom-button').addClass('animated fadeIn');
    }, { offset: '50%' });
    $('#animate-bottom-sec-3').waypoint(function() {
        $('#animate-bottom-sec-3').addClass('animated fadeIn');
        $('#animate-bottom-line-3').addClass('animated fadeIn');
    }, { offset: '80%' });
 
});