list_order_details = JSON.parse($('#order_details').val());
url_loading = $('#loading_gif').attr('href');

$(function () {
    var flag_msg = $('#flag_msg').val();
    if (flag_msg == 'success') $("#alert_success").trigger("click", 'Data berhasil disimpan.');
    else if (flag_msg == 'success_2') $("#alert_success").trigger("click", 'Data berhasil diubah.');
    

    function render() {
        getIndex();
        $('#order_details').val(JSON.stringify(list_order_details));
        var tmpl = $('#order-table').html();
        Mustache.parse(tmpl);
        var data = { item: list_order_details };
        var html = Mustache.render(tmpl, data);
        $('#tbody-order').html(html);
        bind();
        totalPriceOrder();
    }

    function bind() {
        $('#selected_category').on('click', tambahKategori);
        $('.update-total-harga').on('change', updateTotalHarga);
        $('.update-deskripsi-detail-order').on('change', updateDetail);
        $('.update-qty-detail').on('change', updateDetail);
        $('.update-harga-satuan').on('change', updateDetail);
        $('.btn-update-total-harga').on('click', updateTotalHarga);
        $('.btn-save-detail-order').on('click', tambahDetail);
        $('.btn-delete-detail-order').on('click', hapusDetail);
        $('.btn-delete-cetegory').on('click', hapusKategori);

        $('.input-number').keypress(function (e) {
            if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
                return false;
            }
            return true;
        });

        $('.update-total-harga').keypress(function (e) {
            if (e.keyCode == 13) {
                var i = $(this).data('id');

                e.preventDefault();
                $('#btnUpdateTotalHarga_' + i).click();
            }
        });

        $('#category_option').keypress(function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                tambahKategori();
            }
        });

        $('.input-kategori').keypress(function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                tambahKategori();
            }
        });

        /*$('.input-new-detail-order').keypress(function (e) {
            var i = $(this).data('id');
            if (e.keyCode == 13) {
                e.preventDefault();
                $('#save_detail_order_' + i).click();
            }
        });*/

        $('.input-harga-satuan').keypress(function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                //tambahDetail();
            }
        });
    }

    function checkIsExsis(category){
        var flag = 0;
        for (id in list_order_details) {
            var data = list_order_details[id];

            if (category == data.category)
                flag++;
        }

        return flag;
    }

    function tambahKategori() {
        var selected_category = $("#category_option").val();
        if (selected_category == 'DEKORASI' || selected_category == 'RIAS' || selected_category == 'WEDDING_ORGANIZER' ||  selected_category == 'SEWA_GEDUNG') {
            var uom = 'SET';
            var flag_set = true;
            var qty_kategori = 1;
        } else {
            var flag_set = false;
            var qty_kategori = null;
        }


        var harga = $("#harga").val();
        var total_price = (harga) ? parseFloat(harga).toFixed(2) : parseFloat(0).toFixed(2);
        var total_price_format_money = (harga) ? parseFloat(harga).formatMoney() : parseFloat(0).formatMoney();
        var isExists = checkIsExsis(selected_category);

        if (isExists > 0){
            $("#alert_warning").trigger("click", 'Kategori sudah ada');
            return false;
        }

        var input = {
            'id': -1,
            'category': selected_category,
            'qty_kategori': qty_kategori,
            'uom_kategori': uom,
            'total_price': total_price,
            'total_price_format_money': total_price_format_money,
            'flag_set': flag_set,
            'details': []
        };
        list_order_details.push(input);
        render();
    }

    function updateTotalHarga() {
        var i = $(this).data('id');
        var total_harga = $('#updateTotalHarga_' + i).val();
        list_order_details[i].total_price = parseFloat(total_harga).toFixed(2);
        list_order_details[i].total_price_format_money = parseFloat(total_harga).formatMoney();
        render();

    }

    function hapusKategori() {
        var i = $(this).data('id');
        var order_category_id = list_order_details[i].order_category_id;

        if (order_category_id != -1){
            var url_delete_order_category = list_order_details[i].url_delete_order_category;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "delete",
                url: url_delete_order_category
            });
        }

        list_order_details.splice(i, 1);
        render();
    }

    function updateDetail() {
        var i = $(this).data('id');
        var ix = $(this).data('idx');
        var flag_set = list_order_details[i].flag_set;

        if (flag_set) {
            var deskripsi_detail_order = $('#detail_order_input_' + i + '_' + ix).val();
            list_order_details[i].details[ix].detail_order = deskripsi_detail_order;
        } else {
            var qty = parseInt($('#updateQty_' + i + '_' + ix).val());
            var harga_satuan = $('#updateHargaSatuan_' + i + '_' + ix).val();
            var total_all_price = parseFloat(list_order_details[i].total_price);
            var total_price_qty_current = parseFloat(list_order_details[i].details[ix].total_price_qty);
            var total_price = parseInt(qty) * parseFloat(harga_satuan);
            var sum_all = (total_all_price - total_price_qty_current) + total_price;

            list_order_details[i].total_price = parseFloat(sum_all).toFixed(2);
            list_order_details[i].total_price_format_money = parseFloat(sum_all).formatMoney();
            list_order_details[i].details[ix].qty = parseInt(qty);
            list_order_details[i].details[ix].harga_satuan = parseFloat(harga_satuan).toFixed(2);
            list_order_details[i].details[ix].total_price_qty = parseFloat(total_price).toFixed(2);
            list_order_details[i].details[ix].total_price_qty_format_money = parseFloat(total_price).formatMoney();
        }

        render();
    }

    function tambahDetail() {
        var i = $(this).data('id');
        var deskripsi_detail_order = $('#detail_order_' + i).val();
        var flag_set = list_order_details[i].flag_set;

        if (flag_set) {
            var uom = null;
            var qty = null;
            var harga_satuan = null;
            var total_price = null;
        } else {
            var uom = $('#inputUom_' + i).val();
            var qty = parseInt($('#inputQty_' + i).val());
            var harga_satuan = parseFloat($('#hargaSatuanInput_' + i).val());
            var total_all_price = parseFloat(list_order_details[i].total_price);
            var total_price = parseInt(qty) * parseFloat(harga_satuan);
            var sum_all = total_all_price + total_price;
            
            list_order_details[i].total_price = parseFloat(sum_all).toFixed(2);
            list_order_details[i].total_price_format_money = parseFloat(sum_all).formatMoney();
        }

        var input = {
            'id': -1,
            'detail_order': deskripsi_detail_order,
            'uom': uom,
            'qty': qty,
            'harga_satuan': parseFloat(harga_satuan),
            'total_price_qty': parseFloat(total_price),
            'harga_satuan_format_money': parseFloat(harga_satuan).formatMoney(),
            'total_price_qty_format_money': parseFloat(total_price).formatMoney(),
        }

        list_order_details[i].details.push(input);
        render();
    }

    function hapusDetail(){
        var i = $(this).data('id');
        var ix = $(this).data('idx');
        var flag_set = list_order_details[i].flag_set;
        var order_detail_id = list_order_details[i].details[ix].id;
        
        if (flag_set) {
            if (order_detail_id != -1) {
                var url_delete_detail = list_order_details[i].details[ix].url_delete_order_detail;
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "delete",
                    url: url_delete_detail
                });
            }

            list_order_details[i].details.splice(ix, 1);
        } else {
            if (order_detail_id != -1) {
                var url_delete_detail = list_order_details[i].details[ix].url_delete_order_detail;
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "delete",
                    url: url_delete_detail
                });
            }

            var total_all_price = parseFloat(list_order_details[i].total_price);
            var total_price = parseFloat(list_order_details[i].details[ix].total_price_qty);
            var minus_all = total_all_price - total_price;
            list_order_details[i].total_price = parseFloat(minus_all).toFixed(2);
            list_order_details[i].total_price_format_money = parseFloat(minus_all).formatMoney();
            list_order_details[i].details.splice(ix, 1);
        }

       

        render();
    }

    function getIndex() {
        for (id in list_order_details) {
            list_order_details[id]['_id'] = id;
            list_order_details[id]['no'] = parseInt(id) + 1;

            for (var idx in list_order_details[id]['details']) {
                list_order_details[id]['details'][idx]['_idx'] = idx;
                list_order_details[id]['details'][idx]['sub_no'] = parseInt(idx) + 1;
            }
        }
    }

    function totalPriceOrder(){
        var new_total_order = parseFloat(0);

        for (id in list_order_details) {
            var data = list_order_details[id];
            new_total_order += parseFloat(data.total_price);
        }

        $('#total_price_all_category').val(new_total_order);
        $('#total_price_all_category_format_money').val(parseFloat(new_total_order).formatMoney());

        $('#total_order_all').val(new_total_order);
        $('#total_order_all_format_money').val(parseFloat(new_total_order).formatMoney());
        
    }
    
    render();
});

var url_order_index = $('#url_order_index').attr('href');

$('#form').submit(function (event) {
    event.preventDefault();

    var customer_name = $('#customer_name').val();
    var customer_email = $('#customer_email').val();
    var customer_phone_number = $('#customer_phone_number').val();
   
    if (!customer_name) {
        $("#alert_error").trigger("click", 'Silahkan isi nama pelanggan terlebih dahulu.');
        return false;
    }

    if (!customer_email) {
        $("#alert_error").trigger("click", 'Silahkan isi email pelanggan terlebih dahulu.');
        return false;
    }

    if (!customer_phone_number) {
        $("#alert_error").trigger("click", 'Silahkan isi nomor telpon pelanggan terlebih dahulu.');
        return false;
    }

    bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
        if (result) {
            $.ajax({
                type: "POST",
                url: $('#form').attr('action'),
                data: $('#form').serialize(),
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function () {
                    document.location.href = url_order_index;
                },
                error: function () {
                    $.unblockUI();
                    if (response['status'] == 500) {
                        $("#alert_error").trigger("click", 'Please Contact ICT');
                    } else {
                        for (i in response.responseJSON) {
                            $("#alert_error").trigger("click", response.responseJSON[i]);
                        }
                    }
                }
            });
        }
    });




});

function hapus(url) {
    bootbox.confirm("Apakah anda yakin akan menghapus data ini ?.", function (result) {
        if (result) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "DELETE",
                url: url,
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function () {
                    $.unblockUI();
                },
            }).done(function ($result) {
                $("#alert_success").trigger("click", 'Data berhasil dihapus.');
                $('#dataTableBuilder').DataTable().ajax.reload();
            });
        }
    });


}
