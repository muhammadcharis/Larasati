list_venue_packages = JSON.parse($('#venue_packages').val());
url_loading = $('#loading_gif').attr('href');

$(function () {
    function render() {
        getIndex();
        $('#venue_packages').val(JSON.stringify(list_venue_packages));
        var tmpl = $('#venue-table').html();
        Mustache.parse(tmpl);
        var data = { item: list_venue_packages };
        var html = Mustache.render(tmpl, data);
        $('#tbody-venue-package').html(html);
        bind();
    }

    function bind() {
        $('#save_detail_venue').on('click', tambahDetail);
        $('.update-deskripsi-detail-venue').on('change', updateDescription);
        $('.btn-delete-venue_detail').on('click', hapusDetial);
    }



    function tambahDetail() {
        var description = $('#detail_venue').val();
        var input = {
            'venue_detail_id': -1,
            'description': description,
            'url_delete_venue_detail' : null
        };
        list_venue_packages.push(input);
        render();
    }

    function updateDescription() {
        var i = $(this).data('id');
        var description = $('#detail_venue_description_'+i).val();
        list_venue_packages[i].description = description;
        render();

    }

    function hapusDetial() {
        var i = $(this).data('id');
        var venue_detail_id = list_venue_packages[i].venue_detail_id;

        if (venue_detail_id != -1){
            var url_delete_venue_detail = list_venue_packages[i].url_delete_venue_detail;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "delete",
                url: url_delete_venue_detail
            });
        }

        list_venue_packages.splice(i, 1);
        render();
    }

   function getIndex() {
       for (id in list_venue_packages) {
           list_venue_packages[id]['_id'] = id;
           list_venue_packages[id]['no'] = parseInt(id) + 1;
        }
    }

    $('#insert_venue').submit(function (event) {
        event.preventDefault();
    
        var package_name = $('#name').val();
        
        if (!package_name) {
            $("#alert_error").trigger("click", 'Silahkan isi nama paket terlebih dahulu.');
            return false;
        }
    
        bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: $('#insert_venue').attr('action'),
                    data: $('#insert_venue').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                        $('#insertVenueModal').modal('hide');
                    },
                    complete: function () {
                        $.unblockUI();
                        $('#insertVenueModal').modal('hide');
                    },
                    success: function () {
                        list_venue_packages = [];
                        $('#insert_venue').trigger("reset");
                        $('#dataTableBuilder').DataTable().ajax.reload();
                        $("#alert_success").trigger("click", 'Data Berhasil disimpan');
                        render();
                    },
                    error: function () {
                        $.unblockUI();
                        if (response['status'] == 500) {
                            $("#alert_error").trigger("click", 'Please Contact ICT');
                        } else {
                            for (i in response.responseJSON) {
                                $("#alert_error").trigger("click", response.responseJSON[i]);
                            }
                        }
    
                        $('#insertTimelineModal').modal();
                    }
                });
            }
        });
    
    
    
    
    });
    
    function hapus(url) {
        bootbox.confirm("Apakah anda yakin akan menghapus data ini ?.", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
    
                $.ajax({
                    type: "DELETE",
                    url: url,
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    success: function () {
                        $.unblockUI();
                    },
                }).done(function ($result) {
                    $("#alert_success").trigger("click", 'Data berhasil dihapus.');
                    $('#dataTableBuilder').DataTable().ajax.reload();
                });
            }
        });
    
    
    }
    
    render();
});


