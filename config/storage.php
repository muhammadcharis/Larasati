<?php

return [
    'slider' => storage_path() . '/app/slider',
    'portofolio' => storage_path() . '/app/portofolio',
    'blog' => storage_path() . '/app/blog',
    'service' => storage_path() . '/app/service',
    'timeline' => storage_path() . '/app/timeline',
    'gallery_thumbnaile' => storage_path() . '/app/gallery/thumbnail',
    'gallery' => storage_path() . '/app/gallery',
];